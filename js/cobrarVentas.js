$( document ).ready(function(){
  /* ********************************************************
  DataTable
  ********************************************************* */
  var table = $('#tbCobrarVentas').DataTable( {

    language: {
      processing: "<i class='fa fa-spinner fa-5x fa-spin fa-fw' aria-hidden='true'></i>",
      search: "<i class='fa fa-search' aria-hidden='true'></i>",
      lengthMenu:     "Mostrando _MENU_ productos",
      info:           "Mostrando del _START_ al _END_ de _TOTAL_ productos",
      infoEmpty:      "Mostrando 0 al 0 de 0 coincidencias",
      infoFiltered: "(filtrado de un total de _MAX_ elementos)",
      infoPostFix: "",
      loadingRecords: "<i class='fa fa-spinner fa-5x fa-spin fa-fw' aria-hidden='true'></i>",
      zeroRecords: "No se encontraron coincidencias",
      emptyTable: "No hay datos para mostrar",
      paginate: {
        first: "<i class='fa fa-fast-backward fa-lg' aria-hidden='true'></i>",
        previous: "<i class='fa fa-backward fa-lg' aria-hidden='true'></i>",
        next: "<i class='fa fa-forward fa-lg' aria-hidden='true'></i>",
        last: "<i class='fa fa-fast-forward fa-lg' aria-hidden='true'></i>"
      }
      //,
      //aria: {
      //    sortAscending: ": activate to sort column ascending",
      //    sortDescending: ": activate to sort column descending"
      //}
    },
    lengthMenu: [
      [ 5,10, 25, 50, -1 ],
      [ '5','10', '25', '50', 'Todo' ]
    ],
    buttons: [
      { extend: 'colvis', text: '<i class="fa fa-eye" aria-hidden="true"></i>' },
      { extend: 'copy', text: '<i class="fa fa-clipboard" aria-hidden="true"></i>' }, 
      { extend: 'excel', text: '<i class="fa fa-file-excel-o text-success" aria-hidden="true"></i>',title: 'Mi Inventario' }, 
      { extend: 'pdf', text: '<i class="fa fa-file-pdf-o text-danger" aria-hidden="true"></i>',title: 'Mi Inventario' },
      { extend: 'print', text: '<i class="fa fa-print" aria-hidden="true"></i>' }

    ],
    columnDefs:[
      { orderable: false, targets: [0] },{ orderable: false, targets: [4] }
    ],
    order:[
      [ 1, 'asc' ]
    ],
    stateSave: true
  } );

  table.buttons().container().appendTo( $('.col-sm-6:eq(0)', table.table().container() ) );

  
});

/**
 * abrir pagina para cobrar la venta
 *
 * @param int idVenta
 */
function cobrarVenta(idVenta, pagado=false) {
  $("#input-monto").numeric({
    decimalPlaces: 2,
    negative: false
  });
  var estaPagado = '';
  if(pagado){
    estaPagado = '?pagado=1';
  }

  console.log("cobrar vetnas");
  $.ajax({
    url: base_url + 'cobrarventas/formcobrarventa'+estaPagado,
    data: {
      idVenta: idVenta
    },
    type: 'POST',
    success: function (html) {
      $(".modal-header-mini").html(`<span class="text-right" style="font-size:17px">
          <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> 
          Cobrar Venta ` + idVenta + `
        </span>
        <button type="button" class="close" data-dismiss="modal" onclick="location.reload();" aria-label="Close">
          <span aria-hidden="true">
            <font color="#FF0000">
              <i class="fa fa-times" aria-hidden="true"></i>
            </font>
          </span>
        </button><br>`);

      $(".modal-body-mini").html(html);
      $(".modal-footer-mini").html('<button type="button" class="btn btn-default" data-dismiss="modal" >Cerrar</button>');
      $('#ModalMini').modal({
        backdrop: 'static',
        keyboard: false
      });
      $("#ModalMini").modal("show");
      return false;
    },
    error: function (data) {
      alerta('Error interno x7', data['responseText'], '');
    }
  });
}

/**
 * enviar los datos del formulario 
 * cobrar venta
 */
$('body').on('submit', '#formCobrarVenta', function (e) {
  e.preventDefault();
  console.log("enviando datos");
  $.ajax({
    url: base_url + 'cobrarventas/registrarpago',
    data: $(this).serialize(),
    type: 'POST',
    dataType: "json",
    success: function (resp) {
      console.log(resp);
      if  (resp.state){
        //renderizar el formulario de cobro con elboton imprimir
        cobrarVenta($('#idVenta').val(), true);
      }else{
        alert(resp.message);
      }
    },
    error: function (data) {
      alerta('Error interno', data['responseText'], '');
    }
  });
  return false;
});

/*
  * imprimir un comprobante del pago
  * realizado
  */
$("body").on('click', '#impComprobante', function () {
  var idVenta = $("#idVenta").val();
  $.ajax({
    url: base_url + 'cobrarventas/impcomprobante',
    data: {
      idVenta: idVenta
    },
    type: 'POST',
    success: function (html) {
      if (window.print) {
        // ------------------------------------
        var ventimp = window.open(' ', 'popimpr');
        ventimp.document.write(html);
        // ventimp.document.close();
        ventimp.print();
        ventimp.close();
        // ------------------------------------
      } else {
        alert("La función de impresion no esta soportada por su navegador.");
      }
    }
  });
  // location.reload();
});