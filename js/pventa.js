// *********************************************
// globals flags
// *********************************************
var flagNewRegister = false;

$( document ).ready(function(){
  //campos numericos
  // $("#codigo").numeric({ decimal: false, negative: false });
  $("#cantidad").numeric({ decimalPlaces: 3, negative: false });

  /*
  * despues del modal de cobranzan este bloque 
  * envia el importe total para finalizar la venta
  */
  $(document).on('submit', '#formrecibido', function () {
    //establecer labandera con o sin factura
    var yesItFactura = "Sin Factura"
    if ( $("#input-factura").prop("checked") == true ) {
      yesItFactura = "Con Factura";
    }
    //enviando los datos del improte toal para concretarla venta
    $.ajax({
      url: base_url + 'pventa/concretarVenta',
      data: $(this).serialize()+"&idcliente="+$("#input-idcliente").val()+"&factura="+yesItFactura,
      type: 'POST',
      dataType: "json",
      success: function (resp) {
        if(resp['cambio']<0 && resp['cobro_credito'] == 0)
        {
          $("#cobrar").click();
          return false;
        } else {
          $("#libuscar").addClass("hidden");
          $("#general").html('<center><button type="button" class="btn btn-default" onclick="location.reload();">Venta finalizada</button><center>');
          $(".modal-header-mini").html(`<span class="text-right" style="font-size:17px">
              <i class="fa fa-money" aria-hidden="true"></i> Cambio
            </span>
            <button type="button" class="close" data-dismiss="modal" onclick="location.reload();" aria-label="Close">
              <span aria-hidden="true">
                <font color="#FF0000">
                  <i class="fa fa-times" aria-hidden="true"></i>
                </font>
              </span>
            </button><br>`);

          var htmlCambio = "";
          if(impuesto)
          {
            htmlCambio += `<center>
              <h3 class='sin-margen'>Gracias por su compra</h3>
              <label class='text-success sin-margen'>Total: `+monedaString + new oNumero(resp['total']).formato(2, true)+`</label> <br>
              <label class='text-warning sin-margen'>Recibido: `+monedaString + new oNumero(resp['recibido']).formato(2, true)+`</label> <br>`;

            if (resp['cobro_credito'] == 1) {
              console.log("ingresando credito de verdad ")
              // var porCobrar = new oNumero(resp['cambio']).formato(2, true);
              var porCobrar = parseFloat(resp['cambio']);
              porCobrar *= -1;
              htmlCambio += `<label class='text-danger sin-margen'>Deuda por cobrar: `+monedaString + porCobrar+`</label> <br>`;
            } else {
              htmlCambio += `<label class='text-primary sin-margen'>Cambio: `+monedaString + new oNumero(resp['cambio']).formato(2, true)+`</label> <br>`;
            }
            
            htmlCambio += `<button type=\"button\" class=\"btn btn-negro\ impVenta\">
                <i class=\"fa fa-print\" aria-hidden=\"true\"></i> Imprimir
              </button>
            </center>`;

            $(".modal-body-mini").html(htmlCambio);
          }
          else {
            htmlCambio += `<center>
              <h2>Gracias por su compra</h2>
              <label class='text-success'>Total: $`+new oNumero(resp['total']).formato(2, true)+`</label> <br>
              <label class='text-warning'>Recibido: $`+new oNumero(resp['recibido']).formato(2, true)+`</label><br>`;
              
            if (resp['cobro_credito'] == 1){
              console.log("ingresando credito ")
              var porCobrar = new oNumero(resp['cambio']).formato(2, true);
              porCobrar *= -1;
              htmlCambio += `<label class='text-danger'>Deuda por cobrar: $`+porCobrar+`</label>`;
            }else{
              htmlCambio += `<label class='text-primary'>Cambio: $`+new oNumero(resp['cambio']).formato(2, true)+`</label>`;
            }
            htmlCambio += `</center>`;
            $(".modal-body-mini").html(htmlCambio);
          }

          $(".modal-footer-mini").html("");
          $(".modal-footer").html('<button type="button" class="btn btn-default" autofocus onclick="location.reload();">Finalizar</button>');
          $('#ModalMini').modal({
            backdrop: 'static',
            keyboard: false
          });
          $("#ModalMini").modal("show");
          return false;
        }
      },
      error: function (data) {
        alerta('Error interno', data['responseText'], '');
      }
    });
    return false;
  });



  /**
   * realiza el envio delos datos del 
   * descuento para actualizar el etributo 
   * de movimiento invetnario descuento
   */
  $(document).on('submit', '#formdescuento', function (e) {
    e.preventDefault()
    var htmlDescuento = parseFloat($("#input-descuento").val());
    var htmlImporteLimite = parseFloat($("#input-importe-limite").val());
    if ( htmlDescuento >= 0 && htmlDescuento <= 100  ){    
      $.ajax({
        url: base_url + 'pventa/guardardescuento',
        data: $(this).serialize(),
        type: 'POST',
        dataType: "json",
        success: function (resp) {
          console.log(resp);
          $("#tbventa tr").find('td:eq(0)').each(function () {
            //obtenemos el codigo de la celda
            icodigo = $(this).html();
            if(resp.resultadoItem.codigo == icodigo){
              $(this).closest('tr').remove();
            } 
          });
          appendInventario(resp.resultadoItem, resp.precio, resp.monedaString, resp.cantidad, resp.importe, resp.descuento, resp.importeNeto);
          calcularSubtotal();
          $("#ModalMini").modal("toggle");
        },
        error: function (data) {
          alerta('Error interno', data['responseText'], '');
        }
      });
    } else {
      alerta("Mensaje","Descuento no valido.","");
    }
    return false;
  });


  /*
  * funcion para enviar el id venta a renderizar 
  * el comprobante de venta a imrpimir
  */
  $("body").on('click', '.impVenta', function () {
    var idVenta=$("#idVentax").html();
    $.ajax({
      url: base_url + 'pventa/impVenta',
      data: {idVenta:idVenta},
      type: 'POST',
      success: function (html) {
        if (window.print) {
          // ------------------------------------
          var ventimp = window.open(' ', 'popimpr');
          ventimp.document.write( html );
          ventimp.document.close();
          ventimp.print( );
          ventimp.close();
          // ------------------------------------
        } else {
          alert("La función de impresion no esta soportada por su navegador.");
        }
      }
    });
    // location.reload();
  });




  $("body").on('click', '#cobrar', function () {
    var total = $("#miTotalx").html();
    var idVenta=$("#idVentax").html();
    var descuento = $("#input-descuento-venta").val();

    if(impuesto=="1"){
      total=$("#miTotal2x").html();
    }
    total = parseFloat(total).toFixed(2);

    if(total<=0)
    {
      $(".modal-header-mini").html('<span class="text-right" style="font-size:17px"><i class="fa fa-money" aria-hidden="true"></i> Cobrar</span><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><font color="#FF0000"><i class="fa fa-times" aria-hidden="true"></i></font></span></button><br>');
      $(".modal-body-mini").html("<center><h2>Lista Vacia</h2></center>");
      $(".modal-footer-mini").html("");
      $(".modal-footer").html('<button type="button" class="btn btn-default" autofocus data-dismiss="modal">Aceptar</button>');
      $("#ModalMini").modal("show");
      return false;
    }
    $.ajax({
      url: base_url + 'pventa/v_recibido',
      data: {
        total:total,
        idVenta:idVenta,
        impuesto:impuesto,
        descuento:descuento
      },
      type: 'POST',
      success: function (html) {
        $(".modal-header-mini").html('<span class="text-right" style="font-size:17px"><i class="fa fa-money" aria-hidden="true"></i> Cobrar</span><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><font color="#FF0000"><i class="fa fa-times" aria-hidden="true"></i></font></span></button><br>');
        $(".modal-body-mini").html(html);
        $(".modal-footer-mini").html("");
        $('#ModalMini').modal({
          backdrop: 'static',
          keyboard: false
        });
        $("#ModalMini").modal("show");
        $("#recibido").numeric({ decimalPlaces: 2, negative: false });
        $("#recibido").focus();
      }
    });
  });



  $("body").on('click', '#btnNV', function () {
    $.ajax({
      url: base_url + 'pventa/NuevaVenta',
      data: {},
      type: 'POST',
      success: function (idVenta) {
        $("#miVenta").removeClass("hidden");
        $("#idVenta").removeClass("hidden");
        $("#libuscar").removeClass("hidden");
        $("#idVentax").html(idVenta);
        $("#divbtnnv").html("");
      },
      error: function (data) {
        alerta('Error interno x3', data['responseText'], '');
      }
    });


    $("body").on('click', '#buscarpr', function () {
      $.ajax({
        url: base_url + 'pventa/buscarpr',
        data: {},
        type: 'POST',
        success: function (msg) {
          $(".modal-header").html('<span class="text-right" style="font-size:17px"><i class="fa fa-binoculars" aria-hidden="true"></i> Buscar Producto</span><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><font color="#FF0000"><i class="fa fa-times" aria-hidden="true"></i></font></span></button><br>');
          $(".modal-body").html(msg);
          $(".modal-footer").html("");
          callDatatableBuscar();
          $("#Modal").modal("show");
        },
        error: function (data) {
          alerta('Error interno x4', data['responseText'], '');
        }
      });
      return false;
    });



    $("body").on('click', '.addCar', function () {
      // var code=$(this).data("code");
      var code=$(this).data("modelo");
      $("#codigo").val(code);
      $("#Modal").modal("hide");
      addProducto();
      return false;
    });





  });



  /**
   * muestra la la vista de ventas pendientes realizadas a credito
   */
  // $("body").on('click', '#btnCobrarVenta', function () {
  //   $("#divbtnnv").html("");
  //   $("#divCobrarVentas").html("");
  //   $.ajax({
  //     url: base_url + 'pventa/cobrarventa',
  //     data: {},
  //     type: 'POST',
  //     success: function (html) {
  //       // $("#miVenta").removeClass("hidden");
  //       // $("#idVenta").removeClass("hidden");
  //       // $("#libuscar").removeClass("hidden");
  //       // $("#idVentax").html(idVenta);
  //       $("#divCobrarVentas").html(html);
  //     },
  //     error: function (data) {
  //       alerta('Error interno x3', data['responseText'], '');
  //     }
  //   });
  // });


  /* ********************************************************
  detecta nuvo registro ya existente
  ********************************************************* */
  // $("#codigo").delayPasteKeyUp(function(){
  //   addProducto();
  // }, 200);
  $('#codigo').on('keypress', function(e){
    if (e.which == 13) {
      addProducto();
    }
  });


  $('body').on('submit', '#formventaCodigo', function () {
    $("#cantidad").blur();
    $("#codigo").focus();
    return false;
  });




  $(document).on('click', '.delFila', function (event) {
    event.preventDefault();
    var codigo = $(this).closest('tr').find("td")[0].innerHTML;
    var cantidad = $(this).closest('tr').find("td")[3].innerHTML;
    var idVenta = $("#idVentax").html();
    var miTotal = parseFloat($("#miTotalx").html());
    $.ajax({
      url: base_url + 'pventa/importe',
      data: {codigo:codigo,cantidad:cantidad},
      type: 'POST',
      success: function (resultado) {
        var nuevoTotal = miTotal-resultado;
        $("#miTotalx").html(   nuevoTotal   );
        $("#miTotal").html(   new oNumero(nuevoTotal).formato(2, true)   );
        if(impuesto=="1"){
          var totalImpuesto =nuevoTotal*impuestoPorciento/100;
          var miTotal2 = nuevoTotal+totalImpuesto;
          $("#miImpuestox").html(   totalImpuesto   );
          $("#miImpuesto").html(   new oNumero(totalImpuesto).formato(2, true)   );
          $("#miTotal2x").html(   miTotal2   );
          $("#miTotal2").html(   new oNumero(miTotal2).formato(2, true)   );
        }
      },
      error: function (data) {
        alerta('Error interno', data['responseText'], '');
      }
    });
    $.ajax({
      url: base_url + 'pventa/delItemToVenta',
      data: {codigo:codigo,idVenta:idVenta},
      type: 'POST',
      success: function (resultadox2) {
        if(resultadox2!="1")
        {
          alerta("Error al eliminar fila",resultadox2,"");
        }
      },
      error: function (data) {
        alerta('Error interno', data['responseText'], '');
      }
    });
    $(this).closest('tr').remove();
  });

  $('#input-descuento-venta').val(0);


});












// ****************************************************************
// funciones
// ****************************************************************
function addProducto() {
  var codigo = $("#codigo").val();
  var cantida = $("#cantidad").val();
  if(cantida<=0){
    $("#cantidad").val(1);
    $("#codigo").val("");
    return false;
  }
  if(codigo.length>0){
    $.ajax({
      url: base_url + 'pventa/verificarProducto',
      data: {codigo: codigo},
      type: 'POST',
      success: function (resultado) {
        if(resultado)
        {
          $.ajax({
            url: base_url + 'pventa/getItem',
            data: {codigo: codigo},
            type: 'POST',
            dataType: "json",
            success: function (resultadoItem) {
              cantidad = parseFloat(cantida);
              var precio = resultadoItem['precio'];
              var importe = cantidad * parseFloat(resultadoItem['precio']);
              // if(cantidad < resultadoItem['cantidadMayoreo']) {
              // }
              // else{
              //   var importe = cantidad * parseFloat(resultadoItem['precioMayoreo']);
              //   precio = resultadoItem['precioMayoreo'];
              // }

              //console.log('precio');
              //console.log(precio);
              let importeNeto = importe;
              let descuento = 0;
              /**
               * añadiendo los items en la tabla de punto de venta
               */
              // appendInventario(resultadoItem, precio, monedaString, cantidad, importe, descuento, importeNeto);
              
              $("#cantidad").val(1);
              $("#codigo").val("");
              // WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW
              var idVenta=$("#idVentax").html();
              var idItem = resultadoItem['id'];
              var codigo = resultadoItem['codigo'];
              $.ajax({
                url: base_url + 'pventa/addItemToVenta',
                data: {idVenta: idVenta,idItem:idItem, cantidad:cantidad},
                type: 'POST',
                dataType: "json",
                success: function (r) {
                  //si el stock del producto no alcanza o el item no existe
                  if (r['validante'] == "n") {
                    alert("El producto no esta disponible o el stock no cubre la cantidad solicitada");
                  }else{
                    /**
                    * añadiendo los items en la tabla de punto de venta
                    */
                    appendInventario(resultadoItem, precio, monedaString, cantidad, importe, descuento, importeNeto);
                  }

                  //si el producto existe y se agrega nuevamente
                  if(r['validante'] == "r"){
                    $("#tbventa tr").find('td:eq(0)').each(function () {
                      //obtenemos el codigo de la celda
                      icodigo = $(this).html();
                      if(codigo==icodigo){
                        $(this).closest('tr').remove();
                      } 
                    });
                    //actualizamos el importe
                    if(r['ncantidad']<resultadoItem['cantidadMayoreo'])
                    {
                      var importe = r['ncantidad']*parseFloat(resultadoItem['precio']);
                    }
                    else{
                      var importe = r['ncantidad']*parseFloat(resultadoItem['precioMayoreo']);
                      precio = resultadoItem['precioMayoreo'];
                    }

                    /**
                     * actualizar el importe neto de venta
                     */
                    console.log("estableciendo el descuento:");
                    
                    descuento = parseFloat( r['dataMovimiento'].descuento );
                    
                    var desPorcentaje = (importe * descuento) / 100;
                    console.log("el descuento echo es de porcentaje: "+desPorcentaje);
                    // importeNeto = importe - descuento;
                    importeNeto = importe - desPorcentaje;

                    /**
                     * agregamos una nueva fila con el codigo
                     */ 
                    appendInventario(resultadoItem, precio, monedaString, r['ncantidad'], importe, descuento, importeNeto);
                  }
                  // recorremos la tabla para calcular el importe
                  calcularSubtotal();
                },
                error: function (data) {
                  alerta('Error interno x2', data['responseText'], '');
                }
              });
            },
            error: function (data) {
              alerta('Error interno x2', data['responseText'], '');
            }
          });
        }
        else{
          alerta("Mensaje","No hay registros que coincidan con el código ingresado.","");
          $("#codigo").val('');
        }
      },
      error: function (data) {
        alerta('Error interno x1', data['responseText'], '');
      }
    });
  }

}




$.fn.delayPasteKeyUp = function(fn, ms)
{
  var timer = 0;
  $(this).on("propertychange input", function()
  {
    clearTimeout(timer);
    timer = setTimeout(fn, ms);
  });
};


function miAlerta(tipo,titulo,mensaje) {
  var msj='<div class="alert alert-'+tipo+' alert-dismissible" role="alert">'+
  '<button type="button" class="close pull-right" data-dismiss="alert" aria-label="Close"><i class="fa fa-times fa-lg" aria-hidden="true"></i></button>'+
  '<strong>'+titulo+' </strong>'+mensaje+'.'+
  '</div>';
  return msj;
}


function esconderAlerta(){
  setTimeout(function() {
    $("#msj").fadeOut(1500);
  },1500);
  setTimeout(function() {
    $("#msj").html("");
    $("#msj").fadeIn(1);
  },3000);
}



  /* ********************************************************
  Nuevo cliente
  ********************************************************* */
  $('body').on('click', '#btnNuevoCliente', function () {
    $(".modal-header").html('<span class="text-right" style="font-size:17px"><i class="fa fa-user-circle-o fa-lg" aria-hidden="true"></i> Nuevo Cliente</span><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><font color="#FF0000"><i class="fa fa-times" aria-hidden="true"></i></font></span></button><br>');
    $("#ModalClient").modal("show");
  });

  // **************************************************
  // enviar los datos para registrar
  // **************************************************
  $('body').on('submit', '#formAddCliente ', function () {
    $.ajax({
      url: base_url + 'clientes/addCliente',
      data: $(this).serialize(),
      type: 'POST',
      success: function (resultado) {
        console.log(resultado);
        $('#formAddCliente')[0].reset();
        // hide form new cliente
        $("#collapsenewcliente").collapse("hide");
        $("#collapseshowcliente").collapse("show");

        // buscar mostrar clientes
        flagNewRegister = true; 
        $("#formSearchCliente").submit();
      },
      error: function (data) {
        alerta('Error interno', data['responseText'], '');
      }
    });
    return false;
  });

  
  /* ********************************************************
  form buscar clientes
  ********************************************************* */
  $('body').on('submit', '#formSearchCliente', function () {
    $.ajax({
      url: base_url + 'clientes/showajaxclientes',
      data: $(this).serialize(),
      type: 'POST',
      dataType: 'JSON',
      success: function (resultado) {
        if ( resultado.length > 0 ) {
          var res = "";
          for (var i = 0; i < resultado.length; i++) {
            res += "<tr onclick=\"selectIdCliente("+resultado[i]['id']+", '"+resultado[i]['nombre']+"', '"+resultado[i]['apellido']+"' )\" >\
              <td class='text-center'><i class='fa fa-user-circle-o fa-lg' aria-hidden='true'></i></td>\
              <td> "+resultado[i]['nombre']+" "+resultado[i]['apellido']+"</td>\
              <td> "+resultado[i]['direccion']+" </td>\
              <td> "+resultado[i]['nitci']+" </td>\
            </tr>";
          }
          $("#tbody-datos-cliente").html("");
          $("#tbody-datos-cliente").append(res);
          // limipando los valores del formulario
          $('#formSearchCliente')[0].reset();

          if (flagNewRegister) {
            $("#tbody-datos-cliente").children('tr:first').css('background', '#85ff52');
          }
        }else{
          flagNewRegister = false;
          // clona los valores del formulario al from new cliente
          clonarValoresForm();

          $("#tbody-datos-cliente").html("");
          $("#tbody-datos-cliente").append("<tr>\
              <td class='text-center' colspan='4'>No se encontro resultados</td>\
            </tr>");

          // show form new cliente and hide form search cliente
          $("#collapsenewcliente").collapse("show");
          $("#collapseshowcliente").collapse("hide");
        }
      },
      error: function (data) {
        alerta('Error interno', data['responseText'], '');
      }
    });
    return false;
  });

  // ******************************************************
  // selecti fist input nombre in formaddcliente
  // ******************************************************
  $('#input-nombre-formadd').on('input', function () {
    $('#input-nombre-formsearch').val(this.value);
  });

  function selectIdCliente(paramIdCliente, paramNombre, paramApellido){
    var idCliente = parseInt(paramIdCliente);
    $("#input-idcliente").val(idCliente);
    $("#ModalClient").modal("toggle");
    $("#btnNuevoCliente").html("");
    var resIcon = "<span class='glyphicon glyphicon-ok-sign'></span>"
    $("#btnNuevoCliente").append(resIcon+" "+paramNombre+" "+paramApellido);
  }

  // ************************************************************
  // clona los valores del formulario buscar al formulario de 
  // neuvo cliente
  // ************************************************************
  function clonarValoresForm(){
    $("#formSearchCliente input").each(function(indice){
      console.log(this.value);
      $("#formAddCliente input")[indice].value = this.value;
    });
  }

  // ******************************************************
  // funcion par mostrar o ocultar elormulario de new cliente
  // ******************************************************
  $("#button-new-cliente").on('click', function(){
    if (flagNewRegister == false) {
      flagNewRegister = true;
      $("#collapsenewcliente").collapse("show");
      $("#collapseshowcliente").collapse("hide");
    }else{
      flagNewRegister = false;
      $("#collapsenewcliente").collapse("hide");
      $("#collapseshowcliente").collapse("show");
    }
  });

  /**
   * calcular el subtotal segun
   * el importe y el descuento
   */
   // function calcularImporteNeto(el){
   //  var importe = parseFloat( $(el).closest('tr').find('.importe').html() );
   //  var descuento = parseFloat( $(el).closest('tr').find('.descuento').eq(0).val() );
   //  let resultado = importe;
   //  if ( importe >= descuento && descuento >= 0 ){
   //    resultado = importe - descuento; 
   //  }else{
   //    $(el).closest('tr').find('.descuento').eq(0).val("")  
   //  }
   //  $(el).closest('tr').find('.subtotal').html(resultado);

   //  // seteando el sub total
   //  calcularSubtotal();
   // }


  /**
   * realiza la actualizacion del atributo des descuento
   * abre el modal para ingresar el descuento
   */
  function setDescuento(idInventario){
    $.ajax({
      url: base_url + 'pventa/addDescuento',
      data: {
        "idVenta": $("#idVentax").html(),
        "idInventario": idInventario
      },
      type: 'POST',
      success: function (resultado) {
        if ( resultado ){
          $(".modal-header-mini").html(`<span class="text-right" style="font-size:17px">
              <i class="fa fa-money" aria-hidden="true"></i> 
              Descuento
            </span>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">
                <font color="#FF0000">
                  <i class="fa fa-times" aria-hidden="true"></i>
                </font>
              </span>
            </button>
            <br>`);

          $(".modal-body-mini").html(resultado);

          $(".modal-footer-mini").html("");
          $('#ModalMini').modal({
            backdrop: 'static',
            keyboard: false
          });
          $("#ModalMini").modal("show");
        }
      },
      error: function (data) {
        alerta('Error interno', data['responseText'], '');
      }
    });
    return false;
  }

  /**
   * actualiza el atributo factura dela 
   * tabala de movimiento venta
   */
  function setFactura(idInventario, el){
    let  conSinFactura = "Sin factura";
    console.log(el.checked);
    if ( el.checked == true ) {
      conSinFactura = "Con factura";
    }

    $.ajax({
      url: base_url + 'pventa/setFactura',
      data: {
        "idVenta": $("#idVentax").html(),
        "idInventario": idInventario,
        "factura":conSinFactura
      },
      type: 'POST',
      success: function (resultado) {
        console.log(resultado);
        // if ( resultado ){
        //   $(".modal-header-mini").html('<span class="text-right" style="font-size:17px"><i class="fa fa-money" aria-hidden="true"></i> Descuento</span><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><font color="#FF0000"><i class="fa fa-times" aria-hidden="true"></i></font></span></button><br>');

        //   $(".modal-body-mini").html(resultado);

        //   $(".modal-footer-mini").html("");
        //   $('#ModalMini').modal({
        //     backdrop: 'static',
        //     keyboard: false
        //   });
        //   $("#ModalMini").modal("show");
        // }
      },
      error: function (data) {
        alerta('Error interno', data['responseText'], '');
      }
    });
    return false;
  }

  /**
   * agregar el codigo html en la tabla de invnetario
   * con datos del inteario psados por get y reutilizado en mas funciones
   */
   function appendInventario(resultadoItem, precio, monedaString, cantidad, importe, descuento, importeNeto){
    let auxChekFactura = "";
    // if (resultadoItem['factura'] == "Con Factura"){
    //   auxChekFactura = "checked";
    // }
    $('#tbventa tr:last').after("<tr>\
      <td>"+resultadoItem['codigo']+"</td>\
      <td>"+resultadoItem['descripcion']+"</td>\
      <td>"+monedaString+" "+precio+"</td>\
      <td>"+cantidad+"</td>\
      <td class='importe'>"+monedaString+" "+new oNumero(importe).formato(2, true)+"</td>\
      <td class='descuento'>\
        <a href='#' style='cursor:pointer;' onclick='setDescuento("+resultadoItem['id']+")'>% "+new oNumero(descuento).formato(2, true)+"</a>\
      </td>\
      <td class='importe-neto'>"+monedaString+" "+new oNumero(importeNeto).formato(2, true)+"</td>\
      <td class='text-center'>\
        <input type='checkbox' onclick='setFactura("+resultadoItem['id']+", this)' "+auxChekFactura+">\
      </td>\
      <td class='text-center'>\
        <button class='delFila btn btn-danger btn-xs'>\
          <i class='fa fa-times-circle' aria-hidden='true'></i>\
        </button>\
      </td>\
      <td class='hidden'>"+importeNeto+"</td>\
    </tr>");
   }


   /**
    * Calcula el subtotal en base al recorrrido de 
    * la tabla del detalle espesificamente toma el valor de
    * la columna en la pocicion 6
    */
  function calcularSubtotal(){
    let nuevoTotal = sumarImportes();
    $("#miTotalx").html(   nuevoTotal   );
    $("#miTotal").html(   new oNumero(nuevoTotal).formato(2, true)   );

    if ( $("#input-descuento-venta").val() != "" || $("#input-descuento-venta").val() != null ){
      // $("#miTotalx").html(   nuevoTotal   );
      // $("#miTotal").html(   new oNumero(nuevoTotal).formato(2, true)   );
      descuentoSubtotal();
    }

    if(impuesto=="1"){
      var totalImpuesto =nuevoTotal*impuestoPorciento/100;
      var miTotal2 = nuevoTotal+totalImpuesto;
      $("#miImpuestox").html(   totalImpuesto   );
      $("#miImpuesto").html(   new oNumero(totalImpuesto).formato(2, true)   );
      $("#miTotal2x").html(   miTotal2   );
      $("#miTotal2").html(   new oNumero(miTotal2).formato(2, true)   );
    }
  }

  /**
   * suma los importes netos para calcular el subtotal
   */
  function sumarImportes(){
    let nuevoTotal = 0;
    $("#tbventa tr").find('td:eq(9)').each(function () {
      nuevoTotal += parseFloat($(this).html());
    });
    return nuevoTotal;
  }

  /**
   * calcular el subtotal despues del ingresar el descuento
   */
   function descuentoSubtotal(){
    var totalx  = parseFloat( sumarImportes() );
    var totalDescuento  = parseFloat( $("#input-descuento-venta").val() );
    if ( totalDescuento >= 0 && totalDescuento <= 100 ){
      var por = (totalx * totalDescuento)/100;
      var res = totalx - por;
      $("#miTotalx").html(res);
      $("#miTotal").html(res);
    }else{
      $("#miTotalx").html(sumarImportes());
      $("#miTotal").html(sumarImportes());
    }
   }



/* ********************************************************
DataTable
********************************************************* */
function callDatatableBuscar() {


var table = $('#tbBuscar').DataTable( {

  language: {
    processing: "<i class='fa fa-spinner fa-5x fa-spin fa-fw' aria-hidden='true'></i>",
    search: "<i class='fa fa-search' aria-hidden='true'></i>",
    lengthMenu:     "Mostrando _MENU_ productos",
    info:           "Mostrando del _START_ al _END_ de _TOTAL_ productos",
    infoEmpty:      "Mostrando 0 al 0 de 0 coincidencias",
    infoFiltered: "(filtrado de un total de _MAX_ elementos)",
    infoPostFix: "",
    loadingRecords: "<i class='fa fa-spinner fa-5x fa-spin fa-fw' aria-hidden='true'></i>",
    zeroRecords: "No se encontraron coincidencias",
    emptyTable: "No hay datos para mostrar",
    paginate: {
      first: "<i class='fa fa-fast-backward' aria-hidden='true'></i>",
      previous: "<i class='fa fa-backward' aria-hidden='true'></i>",
      next: "<i class='fa fa-forward' aria-hidden='true'></i>",
      last: "<i class='fa fa-fast-forward' aria-hidden='true'></i>"
    }
    //,
    //aria: {
    //    sortAscending: ": activate to sort column ascending",
    //    sortDescending: ": activate to sort column descending"
    //}
  },
  lengthMenu: [
    [ 5 ],
    [ '5' ]
  ],
  columnDefs:[
    { orderable: false, targets: [0] },{ orderable: false, targets: [3] }
  ],
  order:[
    [ 1, 'asc' ]
  ]
} );

}
