<form class='form text-capitalize' id='<?=$idForm?>' autocomplete='off'>
  <input type='hidden' name='idp' value='<?=$idProv?>'/>
  <div class='form-group'>
    <label>Nombre del proveedor</label>
    <input type='text' class='form-control' placeholder='Nombre' name='nombre' maxlength='50' required='required' value='<?=$nombre?>'/>
  </div>

  <div class='form-group'>
      <label>Apellidos</label>
      <input type='text' class='form-control' placeholder='apellidos' name='apellidos' maxlength='20' required='required' value='<?=$proveedor['apellidos']?>'/>
    </div>

  <div class='form-group'>
    <label>RFC</label>
    <input type='text' class='form-control' placeholder='RFC' name='rfc' maxlength='13' required='required' value='<?=$proveedor['rfc']?>'/>
  </div>

  <div class='form-group'>
    <label>Codigo</label>
    <input type='number' min="0" step="1" class='form-control' placeholder='codigo' name='codigo' maxlength='10' required='required' value='<?=$proveedor['codigo']?>'/>
  </div>

  <div class='form-group'>
    <label>Razon Social</label>
    <input type='text' class='form-control' placeholder='razon_social' name='razon_social' maxlength='45' required='required' value='<?=$proveedor['razon_social']?>'/>
  </div>
  <div class='form-group'>
    <label>Celular</label>
    <input type='text' class='form-control' placeholder='celular' name='celular' maxlength='10' required='required' value='<?=$proveedor['celular']?>'/>
  </div>
  <div class='form-group'>
    <label>email</label>
    <input type='email' class='form-control' placeholder='email' name='email' maxlength='20' required='required' value='<?=$proveedor['email']?>'/>
  </div>
  <div class='form-group'>
    <label>Teléfono</label>
    <input type='number' min="0" step="1" class='form-control' placeholder='0000000000' name='tel' maxlength='10' required='required' value='<?=$proveedor['telefono']?>'/>
  </div>
  <div class='form-group'>
    <label>Teléfono2</label>
    <input type='number' min="0" step="1" class='form-control' placeholder='0000000000' name='tel2' maxlength='10' required='required' value='<?=$proveedor['telefono2']?>'/>
  </div>
  <div class='form-group'>
    <label>Teléfono3</label>
    <input type='number' min="0" step="1" class='form-control' placeholder='0000000000' name='tel3' maxlength='10' required='required' value='<?=$proveedor['telefono3']?>'/>
  </div>
  <div class='form-group'>
    <label>Teléfono4</label>
    <input type='number' min="0" step="1" class='form-control' placeholder='0000000000' name='tel4' maxlength='10' required='required' value='<?=$proveedor['telefono4']?>'/>
  </div>
  <div class='form-group'>
    <label>Direccion</label>
    <input type='text' class='form-control' placeholder='direccion' name='direccion' maxlength='250' required='required' value='<?=$proveedor['direccion']?>'/>
  </div>
  <div class='form-group'>
    <label>Pais</label>
    <input type='text' class='form-control' placeholder='pais' name='pais' maxlength='20' required='required' value='<?=$proveedor['pais']?>'/>
  </div>
  <div class='form-group'>
    <label>Ciudad</label>
    <input type='text' class='form-control' placeholder='ciudad' name='ciudad' maxlength='20' required='required' value='<?=$proveedor['ciudad']?>'/>
  </div>
  <div class='form-group'>
    <label>Nit</label>
    <input type='number' min="0" step="1" class='form-control' placeholder='nit' name='nit' maxlength='10' required='required' value='<?=$proveedor['nit']?>'/>
  </div>
  <div class='form-group'>
    <label>Observaciones</label>
    <input type='textarea' class='form-control' placeholder='observacion' name='observacion' maxlength='250' required='required' value='<?=$proveedor['observacion']?>'/>
  </div>





  <br/><br/>
  <button type='submit' class='btn btn-success pull-right'><?=$textoBoton?></button>
  <div class='pull-right'>&nbsp;&nbsp;&nbsp;&nbsp;</div>
  <button type='button' class='btn btn-default pull-right' data-dismiss='modal'>Cancelar</button>
</form>
