<html>
  <head></head>
  <body>
    <style>
      .sin-margen{
      margin: 0;
      padding: 0;
      }
      .text-center{
      text-align: center;
      }
      .text-right{
      text-align: right;
      }
      .text-left{
      text-align: left;
      }
      .table{
        padding:0;
        margin:0;
      }
      .table-bordered tbody tr td,th{
        padding:2px;
        font-size:11px;
        border-left:1px solid black;
        /* border-top:1px solid black; */
        border-bottom:1px solid black;
      }
      .final-td{
        border-right:1px solid black;
      }
      .bordered{
        border:1px solid black;
      }
      table tbody tr td,th{
        padding:2px;
        font-size:11px;
      }
      .space-block{
        width:100%;
        display:block;
        height:3px;
      }
      .space-block10{
        width:100%;
        display:block;
        height:50px;
      }
    </style>

    <table class="table bordered" cellspacing="0">
      <tbody>
        <tr>
          <td class="text-center" style="width:72mm">
            <?=$nombreEmpresa?> <br>
            Av. Villazon km 5 C/Innominad <br>
            4544825 - Fax 4233082
          </td>
          <td class="text-center" style="width:84mm">
            <h2 style="font-style:italic">
              NOTA DE ENTREGA
            </h2> 
            (En dolares) <br>
            SUCURSAL: CENTRAL 
          </td>
          <td class="text-right" style="width:60mm">
            <?=date('d-m-Y', strtotime($detalleVenta['Fecha']))?>
            <?=date('H:i:s', strtotime($detalleVenta['Fecha']))?>
          </td>
        </tr>
      </tbody>
    </table>

    <div class="space-block"></div>

    <table class="table bordered" cellspacing="0">
      <tbody>
        <tr>
          <td class="text-left" style="width:72mm">
            <b>Codigo:</b> <?=$cliente?$cliente['codigo']:''?> <br>
            <!-- <b>Codigo:</b> <?=$detalleVenta['id']?> <br> -->
            <b>Cliente:</b> <?=$cliente?$cliente['nombre_comercial']:''?> <br>
            <!-- <b>Contacto:</b> <?=$cliente?$cliente['contacto']:''?> <br> -->
            <b>Factura:</b> <?=$cliente?$cliente['contacto']:''?> <br>
            <b>NIT:</b> <?=$cliente?$cliente['nitci']:''?> <br>
            <b>Dirección:</b> <?=$cliente?$cliente['direccion']:''?> <br>
            <b>Teléfono:</b> <?=$cliente?$cliente['telefono']:''?> <br>
          </td>
           <td class="text-left" style="width:72mm">
           <!--  <b>Vendedor:</b> <?=$vendedor['nombre']?> <br>
            <b>NIT Cliente:</b> <?=$cliente?$cliente['nitci']:''?> -->
          </td>
          <td class="text-right" style="width:72mm">
            <!-- <b>Numero:</b> -->
            <b>Codigo:</b> <?=$detalleVenta['id']?> <br>
            <b>Forma de pago:</b> <?=$detalleVenta['cobro_credito']==0?'Al contado':'A credito' ?> <br>
            <b>Fecha:</b> <?=date('d-m-Y', strtotime($detalleVenta['Fecha']))?> <br>
            <!-- <b>Fecha Vto:</b> <?=date('d-m-Y', strtotime($detalleVenta['Fecha']))?> <br> -->
            <!-- <b>Factura:</b> <?=$detalleVenta['factura']?> <br> -->
            <b>Destino:</b> <?=$cliente?$cliente['ciudad']:''?> <br>
            <b>Vendedor:</b> <?=$vendedor['nombre']?> <br>
          </td>
        </tr>
      </tbody>
    </table>
    
    <table class="table-bordered" cellspacing="0">
      <thead>
        <tr>
          <th style="width:50px"><span>Código</span></th>
          <th><span>Cantidad</span></th>
          <th><span>Unidad</span></th>
          <th><span>Descripción y marca</span></th>
          <th><span>P.U.</span></th>
          <th><span>% Desc.</span></th>
          <th><span>P.U.</span></th>
          <th class="final-td"><span>Importe Neto</span></th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($detalle as $key => $item) { ?>
          <tr>
            <td style="width:50px">
              <?=$item['producto']['modelo']?>
            </td>
            <td>
              <?=$item['cantidad']?>
            </td>
            <td>
              <?=$item['producto']['objTipo']['nombreTipo']?>
            </td>
            <td>
              <?=$item['producto']['descripcion']?>
            </td>
            <td>
              <?=$item['producto']['precio']?>
            </td>
            <td>
              <?=$item['descuento']?>
            </td>
            <td>
              <?=$item['puTotal']?>
            </td>
            <td class="final-td">
              <?=$item['importeNeto']?>
            </td>
          </tr>
        <?php } ?>
          <tr>
            <td  colspan="4" style="width:108mm;border-right:0;border-bottom:0;" class="text-left">
              <h4>Totales</h4>
            </td>
            <td  colspan="4" style="width:108mm; border-left:0;border-right:1px solid black;border-bottom:0;" class="text-right">
              <?=$detalleVenta['ventaTotalNeto']?>
            </td>
          </tr>
          <tr>
            <td colspan="8" style="width:108mm;text-transform:uppercase;border-right:1px solid black;" class="text-left">
              SON: <?=$totalLiteral?>
            </td>
          </tr>
      </tbody>
    </table>

    <div class="space-block"></div>

    <table class="table bordered" cellspacing="0">
      <tbody>
        <tr>
          <td colspan="3">
            Glosa: MUESTRA (LA PAZ) SUP. SANTOS ZACARIAS AUT. LUIS TORREZ<br>
            Nota: La mercaderia Viaja por Cuenta y Riesgo del comprador
          </td>
        </tr>
        
        <tr>
          <td colspan="3">
            <div class="space-block10"></div>
          </td>
        </tr>

        <tr>
          <td class="text-center" style="width:72mm">
          _______________________________ <br>
            Elaborado por
          </td>
          <td class="text-center" style="width:72mm">
          _______________________________ <br>
            Despachado por
          </td>
          <td class="text-center" style="width:72mm">
            _______________________________ <br>
            Recibí conforme
          </td>
        </tr>
      </tbody>
    </table>

  </body>
</html>