





<!-- ******************************************************************************************************************* -->
<!-- ******************************************************************************************************************* -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Modificar elemento
      <small>Inventario</small>
    </h1>
    <!-- <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol> -->
</section>

<!-- Main content -->
<section class="content">
  <div id="msj"></div>
  <!-- ========================================================================================================================= -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="box box-warning">
        <div class="box-header">
          <h3 class="box-title">Modificar elemento</h3>
        </div>
        <div class="box-body">
          <form id="modItem" autocomplete="off">
            <input type="hidden" name="id" value="<?=$item['id']?>">
            <div class="col-md-3 col-lg-2 col-xs-12">
              <label for="codigo">Código de producto:</label>
            </div>

            <div class="col-md-4 col-lg-4 col-xs-12">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-barcode" aria-hidden="true"></i></span>
                <input type="text" class="form-control" placeholder="Código" maxlength="13" name="codigo" id="codigo" value="<?=$item['codigo']?>" required="required" readonly="readonly">
                <!-- <span class="input-group-btn">
                <button type="button" class="btn btn-info  buscarItem"><i class="fa fa-search" aria-hidden="true"></i></button>
              </span> -->
            </div>
          </div>
            <input id="hidden-value-estado" type="hidden" value="<?=$item['estado']?>">
            <input id="input-estado" type="checkbox" name="estado">

          <div class="clearfix"></div><br>

          <div class="col-md-3 col-lg-2 col-xs-12">
            <label for="codigo">Descripción del producto:</label>
          </div>
          <div class="col-md-9 col-lg-9 col-xs-12">
            <input type="text" class="form-control input-sm" placeholder="Descripción del producto" maxlength="60" name="descripcion" id="descripcion" required="required" value="<?=$item['descripcion']?>">
          </div>

          <div class="clearfix"></div><br>

          <div class="col-md-3 col-lg-2 col-xs-12">
            <label for="codigo">Costo:</label>
          </div>
          <div class="col-md-2 col-lg-2 col-xs-12">
            <div class="input-group">
              <span class="input-group-addon"><b><?=$monedaString?></b></span>
              <input type="text" class="form-control input-sm" placeholder="0.00" maxlength="13" name="costo" id="costo" required="required" value="<?=$item['costo']?>">
            </div>
          </div>

          <div class="clearfix"></div><br>

          <div class="col-md-3 col-lg-2 col-xs-12">
            <label for="codigo">Precio al publico:</label>
          </div>
          <div class="col-md-2 col-lg-2 col-xs-12">
            <div class="input-group">
              <span class="input-group-addon"><b><?=$monedaString?></b></span>
              <input type="text" class="form-control input-sm" placeholder="0.00"  name="precio" id="precio" required="required" value="<?=$item['precio']?>">
            </div>
          </div>

          <div class="clearfix"></div><br>

          <div class="col-md-3 col-lg-2 col-xs-12">
            <label for="codigo">Precio al Mayoreo:</label>
          </div>
          <div class="col-md-2 col-lg-2 col-xs-12">
            <div class="input-group">
              <span class="input-group-addon"><b><?=$monedaString?></b></span>
              <input type="text" class="form-control input-sm" placeholder="0.00"  name="pmayoreo" id="pmayoreo" required="required" value="<?=$item['precioMayoreo']?>">
            </div>
          </div>

          <div class="clearfix"></div><br>

          <div class="col-md-3 col-lg-2 col-xs-12">
            <label for="codigo">Cantidad para Mayoreo:</label>
          </div>
          <div class="col-md-2 col-lg-2 col-xs-12">
            <div class="input-group">
              <!-- <span class="input-group-addon"><i class="fa fa-bolt" aria-hidden="true"></i></span> -->
              <input type="text" class="form-control input-sm" placeholder="0"  name="cmayoreo" id="cmayoreo" required="required"  value="<?=$item['cantidadMayoreo']?>">
            </div>
          </div>


       <!--  <div class="clearfix"></div><br>
            <div class="col-md-3 col-lg-2 col-xs-12">
              <label for="codigo">Lista de Precio sugerida venta al publico:</label>
            </div>
            <div class="col-md-3 col-lg-3 col-xs-12">
               <div class="input-group">
              <span class="input-group-addon"><b><?=$monedaString?></b></span>
              <input type="text"  placeholder="0.00"  class="form-control" name="lista_seguridad_v_p" id="lista_seguridad_v_p" required="required" value="<?=$item['lista_seguridad_v_p']?>"></div>
            </div>


            <div class="clearfix"></div><br>
            <div class="col-md-3 col-lg-2 col-xs-12">
              <label for="codigo">PVF sugerido Bolivia:</label>
            </div>
            <div class="col-md-3 col-lg-3 col-xs-12">
               <div class="input-group">
              <span class="input-group-addon"><b><?=$monedaString?></b></span>
              <input type="text"  placeholder="0.00"  class="form-control" name="pvf" id="pvf" required="required" value="<?=$item['pvf']?>"></div>
            </div> -->

          <div class="clearfix"></div><br>

            <div class="col-md-3 col-lg-2 col-xs-12">
              <label for="codigo">Limite Stock:</label>
            </div>
            <div class="col-md-2 col-lg-2 col-xs-12">
              <div class="input-group">
                <!-- <span class="input-group-addon"><i class="fa fa-bolt" aria-hidden="true"></i></span> -->
                <input type="text" class="form-control" placeholder="0"  name="lstock" id="lstock" required="required" value="<?=$item['limiteStock']?>">
              </div>
            </div>
          <!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
          <div class="clearfix"></div><br>

          <div class="col-md-3 col-lg-2 col-xs-12">
            <label for="codigo">U.M</label>
          </div>
          <div class="col-md-2 col-lg-2 col-xs-12">
            <select class="form-control input-sm" name="tipoVenta">
              <?php
              foreach ($tiposVenta as $tipo) {
                $select="";
                if($tipo['id']==$item['idTipo'])
                {
                  $select="selected";
                }
                ?>
                <option  value="<?=$tipo['id']?>"  <?=$select?>><?=$tipo['nombreTipo']?></option>
                <?php
              }
              ?>
            </select>
          </div>
          <!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

          <div class="clearfix"></div><br>

          <div class="col-md-3 col-lg-2 col-xs-12">
            <label for="codigo">Sección:</label>
          </div>
          <div class="col-md-2 col-lg-2 col-xs-12">
            <select class="form-control input-sm" name="departamento">
              <?php
              foreach ($departamentos as $departamento) {
                $select="";
                if($departamento['id']==$item['idDepartamento'])
                {
                  $select="selected";
                }
                ?>
                <option value="<?=$departamento['id']?>" <?=$select?>><?=$departamento['departamento']?></option>
                <?php
              }
              ?>
            </select>
          </div>

            <div class="clearfix"></div><br>
            <div class="col-md-3 col-lg-2 col-xs-12">
              <label for="codigo">modelo:</label>
            </div>
            <div class="col-md-3 col-lg-3 col-xs-12">
              <input type="text" class="form-control" placeholder="modelo"  name="modelo" id="lstock" required="required" value="<?=$item['modelo']?>">
            </div>
            
            <div class="clearfix"></div><br>
            <div class="col-md-3 col-lg-2 col-xs-12">
              <label for="codigo">master:</label>
            </div>
            <div class="col-md-3 col-lg-3 col-xs-12">
              <input type="text" class="form-control" placeholder="master"  name="master" id="lstock" required="required" value="<?=$item['master']?>">
            </div>
            
          <!--   <div class="clearfix"></div><br>
            <div class="col-md-3 col-lg-2 col-xs-12">
              <label for="codigo">codigo sat:</label>
            </div>
            <div class="col-md-3 col-lg-3 col-xs-12">
              <input type="text" class="form-control" placeholder="codigo_sat"  name="codigo_sat" id="lstock" required="required" value="<?=$item['codigo_sat']?>">
            </div>

         


            <div class="clearfix"></div><br>
            <div class="col-md-3 col-lg-2 col-xs-12">
              <label for="codigo">pag 2:</label>
            </div>
            <div class="col-md-3 col-lg-3 col-xs-12">
              <input type="text" class="form-control" placeholder="pag2"  name="pag2" id="lstock" required="required" value="<?=$item['pag2']?>">
            </div>
            
            <div class="clearfix"></div><br>
            <div class="col-md-3 col-lg-2 col-xs-12">
              <label for="codigo">Division Catalogo fama:</label>
            </div>
            <div class="col-md-3 col-lg-3 col-xs-12">
              <input type="text" class="form-control" placeholder="division_catalogo_fama"  name="division_catalogo_fama" id="lstock" required="required" value="<?=$item['division_catalogo_fama']?>">
            </div> -->
          <div class="clearfix"></div><br>

          <div class="col-md-7 col-lg-7 col-xs-12">
            <button type="submit" class="btn btn-block btn-warning btn-lg"><i class="fa fa-pencil" aria-hidden="true"></i> Modificar Elemento</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- ========================================================================================================================= -->

<!-- Your Page Content Here -->

</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- ******************************************************************************************************************* -->
<!-- ******************************************************************************************************************* -->
