<center>
  <!-- <h2>Total: <?=$monedaString?><?=number_format($total,2)?></h2> -->
  <form class="form form-inline" action="" method="post" id="formdescuento" autocomplete="off">
    <input type="hidden" name="idMovimientoInventario" value="<?=$idMovimientoInventario?>">
    <input type="hidden" name="idItem" value="<?=$idItem?>">
    <input type="hidden" name="idVenta" value="<?=$idVenta?>">
    <input type="hidden" name="precio" value="<?=$precio?>">
    <input id="input-importe-limite" type="hidden" name="importe" value="<?=$importe?>">

    <div class="form-group">
      <label for="recibido">Descuento:</label>
      <div class="input-group">
        <div class="input-group-addon"><b><?=$monedaString?></b></div>
        <input id="input-descuento" name='descuento' type="text" class="form-control" autofocus required="required" value="<?=$descuento?>">
      </div>
    </div>

    <br>
    <br>
    <button type="submit" class="btn btn-success btn-block">Continuar</button>
  </form>
</center>
