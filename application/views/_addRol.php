<!-- <?php
print_r($datos)
?> -->
<form class='form text-capitalize' id='<?=$idForm?>' autocomplete='off'>
  <input type='hidden' name='idp' value='<?=$idRol?>'/>
  <div class='form-group'>
    <label>Nombre Rol</label>
    <input type='text' class='form-control' placeholder='rol' name='rol' maxlength='100' required='required' value='<?=$rol?>'/>
  </div>
  <br/><br/>
  <button type='submit' class='btn btn-success pull-right'><?=$textoBoton?></button>
  <div class='pull-right'>&nbsp;&nbsp;&nbsp;&nbsp;</div>
  <button type='button' class='btn btn-default pull-right' data-dismiss='modal'>Cancelar</button>
</form>
