<div class="text-center">
  <h2>Total: <?=$monedaString?><?=number_format($total,2)?></h2>
  <form class="form form-inline" action="" method="post" id="formrecibido" autocomplete="off">
    <input type="hidden" name="idVenta" value="<?=$idVenta?>">
    <input type="hidden" name="total" value="<?=$total?>">
    <input type="hidden" name="impuesto" value="<?=$impuesto?>">
    <input type="hidden" name="descuento" value="<?=$descuento?>">
    <?php
    if($impuesto)
    {
      ?>
      <input type="hidden" name="impuestoPorciento" value="<?=$impuestoPorciento?>">
      <input type="hidden" name="nombreImpuesto" value="<?=$nombreImpuesto?>">
      <?php
    }
    ?>
    
    <!-- Elegir si es a credito o al contado -->
    <hr>
    <div class="form-group">
      <label for="cobro_credito">Cobrar a credito</label>
      <input id="cobro_credito" type="checkbox" name="cobro_credito">
    </div>
    <div id="mensaje-credito-si" class="alert alert-info" style="display:none;">
      <b>Cobro a credito esta activado</b> Ingrese el monto inicial, posteriormente se puede realizar los otros cobros en el menu cobros.
    </div>
    <hr>

    <!-- Monto recibido -->
    <div class="form-group">
      <label for="recibido">Recibiendo:</label>
      <div class="input-group">
        <div class="input-group-addon"><b><?=$monedaString?></b></div>
        <input type="text" class="form-control" id="recibido" autofocus="true" name="recibido" maxlength="7" required="required" placeholder="<?=$total?>">
      </div>
    </div>
    <br>
    <br>
    <button type="submit" class="btn btn-success btn-block">Continuar</button>
  </form>
</div>

<script>
  // starting bootstrap switch
  $("#cobro_credito").bootstrapSwitch({
    state: false,
    size: 'small',
    animate: true,
    onColor: 'danger',
    offColor: 'default',
    onText: 'Si',
    offText: 'No',
    onSwitchChange: (e,state) => {
      console.log("Cambiando "+state);
      if (state){
        $("#mensaje-credito-si").css('display', 'block');
      }else{
        $("#mensaje-credito-si").css('display', 'none');
      }
    }
  });
</script>