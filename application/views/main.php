<!-- ******************************************************************************************************************* -->
<!-- ******************************************************************************************************************* -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper ">
  <!-- Main content -->
  <section class="content ">
    <section class="content ">
      <div class="row">
        <div class="box box-success">
          <div class="box-header">
            <h3 class="box-title">Punto de Venta </h3>
            <small class="pull-right hidden" id="idVenta">Id Venta: <span id="idVentax">0</span></small>
          </div>
          <div class="box-body" id="general">            
            <!-- Botones de acceso -->
            <div id="divbtnnv" class="text-center">
                <button type="button" class="btn btn-warning btn-lg" id="btnNV"><i class="fa fa-cart-plus" aria-hidden="true"></i> Nueva venta</button>
                
                <a href="/cobrarventas">
                  <button type="button" class="btn btn-danger btn-lg">
                    <i class="fa fa-money" aria-hidden="true"></i> Cobrar ventas a credito
                  </button>
                </a>
            </div>

            <!-- Nueva venta -->
            <div id="miVenta" class="hidden">
              <div class="col-lg-12 col-xs-12">
                <!-- AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA    -->
                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">

    <!-- ................... -->
                  <div class="col-md-6 col-lg-6 col-sm-12  col-xs-6">
                    <label for="codigo">Cliente:</label>
                  </div>
                  <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                    <div class="input-group">
                      <input type="hidden" id="input-idcliente" name="idcliente" value="0">
                      <button class="btn btn-success btn-lg" id="btnNuevoCliente">
                        <span class="glyphicon glyphicon-user"></span> 
                        Agregar cliente
                      </button>
                      <br/>
                    </div>
                  </div>
                  
                  <form id="formventaCodigo" autocomplete="off">
                    <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                      <label for="codigo">Cantidad:</label>
                    </div>
                    <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-times fa-lg" aria-hidden="true"></i></span>
                        <input type="text" class="form-control input-sm" placeholder="1" maxlength="10" value="1" name="cantidad" id="cantidad">
                      </div>
                    </div>
                    <!-- ................... -->
                    <div class="clearfix"></div>
                    <br>
                    <!-- ................... -->
                    <div class="col-md-6 col-lg-6 col-sm-12  col-xs-6">
                      <label for="codigo">Modelo del producto:</label>
                    </div>
                    <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-tag" aria-hidden="true"></i></span>
                        <input type="text" class="form-control  input-sm" placeholder="Modelo" autofocus="" maxlength="13" name="codigo" id="codigo">
                      </div>
                      <br/>
                    </div>
                  </form>

              
                  <!-- ................... -->
                  <div class="clearfix"></div>
                  <br>
                </div>
                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                  <div class="total pull-right text-right">
                    <!-- con sin factura com odato 
                    <div class="inline-form" style="font-size:14px">
                      Factura:
                      <input type="radio" id="input-factura" name="input-factura" value="si"> SI /
                      <input type="radio" name="input-factura" value="no" checked="on"> NO
                    </div> -->
                    <hr>

                    Subtotal: <?=$monedaString?> <span id="miTotal">0.00</span><span id="miTotalx" class="hidden">0.00</span>
                    <br>
                    <div class="<?=$impuesto_si?>">
                      <?=$configs->nombreImpuesto?> (<?=$configs->impuestoPorciento?>%): <?=$monedaString?> <span id="miImpuesto">0.00</span><span id="miImpuestox" class="hidden">0.00</span>
                      <br/>
                      <b>Total: <?=$monedaString?> <span id="miTotal2">0.00</span></b><span id="miTotal2x" class="hidden">0.00</span>
                      <br/>
                    </div>
                    <br/>
                    <div class="form-group">
                      Descuento: % 
                      <input type="number" id="input-descuento-venta" placeholder="00.00" oninput="descuentoSubtotal()">
                      <br/>
                    </div>
                    <br/>
                  </div>
                </div>
                <button type="button" id="cobrar" class="btn btn-lg btn-success pull-right alert"><i class="fa fa-check-circle fa-lg" aria-hidden="true"></i> Cobrar</button>
                <!-- AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA    -->
                <br><br>
                <!-- BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB -->
                <!-- <div class="table-responsive"> -->
                  <table id="tbventa" class="table table-striped table-bordered dt-responsive nowrap table-hover table-condensed" cellspacing="0" width="100%" style="background: white!important">
                    <thead>
                      <tr>
                        <th class="bg-primary"><span>Codigo</span></th>
                        <th class="bg-primary"><span>Descripcion</span></th>
                        <th class="bg-primary"><span>Costo Unitario</span></th>
                        <th class="bg-primary"><span>Cantidad</span></th>
                        <th class="bg-primary"><span>Importe</span></th>
                        <th class="bg-primary"><span>Descuento</span></th>
                        <th class="bg-primary"><span>Importe neto</span></th>
                        <th class="bg-primary"><span>Factura</span></th>
                        <th class="bg-primary"><span>Descartar</span></th>
                        <th class="hidden"><span>importex</span></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr></tr>
                    </tbody>
                  </table>
                <!-- </div> -->
                <!-- BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB -->
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </section>
</div>
<!-- modal registrar  nuevo cliente -->
<div id="ModalClient" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body-custom">
        <div class="form-group text-right">
          <button id="button-new-cliente" class="btn btn-xs btn-success">Registrar / buscar cliente</button>
        </div>
        <div class="collapse" id="collapsenewcliente">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col lg-12 col-xl-12 text-center">
              <h4>Registrar cliente</h4>
            </div>
          </div>
          <form class="form text-capitalize" id="formAddCliente" autocomplete="off">
            <div class='form-group'>
              <div class="form-group">
                <label>Nombre(s) del cliente</label>
                <input id="input-nombre-formadd" type='text' class='form-control' placeholder='Nombre' name='nombre' maxlength='50' required='required'/>        
              </div>
              <div class='form-group'>
                <label>Apellido</label>
                <input type='text' class='form-control' placeholder='Apellido Materno' name='apellido' maxlength='20' required='required'/>
              </div>
              <div class='form-group'>
                <label>Direccion</label>
                <input type='text' class='form-control' placeholder='Calle' name='direccion' maxlength='30'/>
              </div>
              <div class='form-group'>
                <label>Nit / CI</label>
                <input type='text' class='form-control' placeholder='Colonia' name='nitci' maxlength='30' />
              </div>
              
              <div class='form-group'>
                <label>codigo</label>
                <input type='text' class='form-control' placeholder='codigo' name='codigo' maxlength='30' />
              </div>
              <div class='form-group'>
                <label>razon social</label>
                <input type='text' class='form-control' placeholder='razon_social' name='razon_social' maxlength='30' />
              </div>
              <div class='form-group'>
                <label>contacto</label>
                <input type='text' class='form-control' placeholder='contacto' name='contacto' maxlength='30' />
              </div>
              <div class='form-group'>
                <label>email</label>
                <input type='email' class='form-control' placeholder='email' name='email' maxlength='30' />
              </div>
              <div class='form-group'>
                <label>telefono</label>
                <input type='number' min="0" step="1" class='form-control' placeholder='telefono' name='telefono' maxlength='30' />
              </div>
              <div class='form-group'>
                <label>ciudad</label>
                <input type='text' class='form-control' placeholder='ciudad' name='ciudad' maxlength='30' />
              </div>

              <div class='form-group'>
                <button type="submit" class="btn btn-success pull-right">
                <span class="glyphicon glyphicon-ok"></span> 
                Agregar
                </button>
                <div class="pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</div>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </form>
        </div>

        <div class="collapse in" id="collapseshowcliente">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col lg-12 col-xl-12 text-center">
              <h4>Buscar y seleccionar cliente</h4>
            </div>
          </div>
          <form id="formSearchCliente">
            <div class="row">
              <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                <div class="form-group">
                  <label>Nombre</label>
                  <input type="search" id="input-nombre-formsearch" placeholder="Nombre del cliente" name="nombre" class="form-control">
                </div>
              </div>
              <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                <div class="form-group">
                  <label>Apellido</label>
                  <input type="search" placeholder="Apellido del cliente" name="apellido" class="form-control">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                <div class="form-group">
                  <label>Direccion</label>
                  <input type="search" placeholder="Direccion del cliente" name="direccion" class="form-control">
                </div>
              </div>
              <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                <div class="form-group">
                  <label>NIT / CI</label>
                  <input type="search" placeholder="Nit / CI del cliente" name="nitci" class="form-control">
                </div>
              </div>
            </div>
            <div class="row text-right">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <button type="submit" class="btn btn-primary"> 
                <span class="glyphicon glyphicon-search"></span> 
                Buscar
                </button>
              </div>
            </div>
          </form>
          <div class="table-responsive">
            <style type="text/css">
              #tbody-datos-cliente tr:hover{
                background-color: #85ff52;
                cursor: pointer;
              }
            </style>
            <table class="table table-hover">
              <thead>
                <th class="bg-primary"><span>&nbsp;</span></th>
                <th class="bg-primary"><span>Nombre</span></th>
                <th class="bg-primary"><span>Dirección</span></th>
                <th class="bg-primary"><span>Nit / CI</span></th>
              </thead>
              <tbody id="tbody-datos-cliente">
                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>

      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  var impuesto = "<?=$configs->impuesto?>";
  if(impuesto){
    var impuestoPorciento="<?=$configs->impuestoPorciento?>";
  }
  var monedaString = "<?=$monedaString?>";
</script>