<div class="content-wrapper">
  <section class="content">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="box box-success">
          <div class="box-header">
            <h3>Ventas por cobrar</h3>
          </div>
          <div class="box-body table-responsive" id="general">   
            <table id="tbCobrarVentas" class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th>#</th>
                  <th>usuario</th>
                  <th>cliente</th>
                  <th>subtotal</th>
                  <th>descuento</th>
                  <th>total</th>
                  <th>Fecha</th>
                  <th>accion</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($ventas as $key=>$venta) { ?>
                  <?php 
                    $ventaTot = floatval($venta['Total']);
                    $ventaPorTot = ( $ventaTot * floatval($venta['descuento']) ) / 100;
                    $venta['ventaTotalNeto'] = $ventaTot - $ventaPorTot;
                  ?>
                  <tr data-id="<?=$venta['id']?>">
                    <td class="text-center"><?=$key+1?></td>
                    <td><?=$venta['nombreUsuario']?></td>
                    <td><?=$venta['nombreCliente']?></td>
                    <td class="text-right"><?=$monedaString?> <?=number_format($venta['Total'],2)?></td>
                    <td class="text-right"><?=$monedaString?> <?=number_format($venta['descuento'],2)?></td>
                    <td class="text-right"><?=$monedaString?> <?=number_format($venta['ventaTotalNeto'],2)?></td>
                    <td class="text-center"><?=date('d/m/Y H:i:s', strtotime($venta['Fecha']))?></td>
                    <td class="text-center">
                      <button class="btn btn-danger" onclick="cobrarVenta(<?=$venta['id']?>)">
                        <i class="glyphicon glyphicon-usd"></i>  Cobrar
                      </button>
                    </td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    </section>
  </div>