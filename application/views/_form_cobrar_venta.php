

<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="form-group">
      <h4>Cliente: <?=$cliente['nombre']?> / <?=$cliente['nombre_comercial']?></h4>
      <h4>Total <?=$monedaString.' '.number_format($venta['Total']+$venta['descuento'],2)?></h4>
      <h4>Descuento <?=$monedaString.' '.number_format($venta['descuento'],2)?></h4>
      <h4>Total neto <?=$monedaString.' '.number_format($venta['Total'],2)?></h4>
      <h4 class="text-success">Pagado <?=$monedaString.' '.number_format($venta['pagado'],2)?></h4>
      <h4 class="text-danger">Por cobrar <?=$monedaString.' '.number_format($venta['porCobrar'],2)?></h4>
    </div>

    <div class="text-center">
      <div class="alert alert-success <?=$pagoTerminado?'':'hidden'?>">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>El pago se realizo correctamente!</strong>
      </div>
      
      <div class="form-group <?=$pagado?'':'hidden'?>">
        <button type="button" class="btn btn-negro" id="impComprobante">
          <i class="fa fa-print" aria-hidden="true"></i> Imprimir comprobante
        </button>
      </div>
    </div>

    <form class="form form-inline" id="formCobrarVenta" autocomplete="off" method="post" action="">
      <input type="hidden" id="idVenta" name="idVenta" value="<?=$venta['id']?>">
      <div class="text-center <?=$pagoTerminado?'hidden':''?>">
        <div class="form-group">
          <label for="monto">Monto</label>
          <input name="monto" class="form-control" id="input-monto" type="text" placeholder="Monto">
        </div>
        <div class="form-group">
          <button class="btn btn-success" type="submit">
            <i class="glyphicon glyphicon-usd"></i> Cobrar
          </button>
        </div>
      </div>
    </form>
    
  </div>
</div>
