<html>
  <head></head>
  <body>
    <style>
      .sin-margen{
      margin: 0;
      padding: 0;
      }
      .text-center{
      text-align: center;
      }
      .text-right{
      text-align: right;
      }
      .text-left{
      text-align: left;
      }
      .table{
        padding:0;
        margin:0;
      }
      .table-bordered tbody tr td,th{
        padding:2px;
        font-size:11px;
        border-left:1px solid black;
        /* border-top:1px solid black; */
        border-bottom:1px solid black;
      }
      .final-td{
        border-right:1px solid black;
      }
      .bordered{
        border:1px solid black;
      }
      table tbody tr td,th{
        padding:2px;
        font-size:11px;
      }
      .space-block{
        width:100%;
        display:block;
        height:3px;
      }
      .space-block10{
        width:100%;
        display:block;
        height:50px;
      }
    </style>

    <table class="table bordered" cellspacing="0">
      <tbody>
        <tr>
          <td class="text-center" style="width:72mm">
            <?=$nombreEmpresa?> <br>
            Av. Villazon km 5 C/Innominad <br>
            4544825 - Fax 4233082
          </td>
          <td class="text-center" style="width:84mm">
            <h2 style="font-style:italic">
              COMPROBANTE DE PAGO
            </h2> 
            (En dolares) <br>
            SUCURSAL: CENTRAL 
          </td>
          <td class="text-right" style="width:60mm">
            Fecha: <?=date('d-m-Y', strtotime($detalleVenta['Fecha']))?> <br>
            Hora: <?=date('H:i:s', strtotime($detalleVenta['Fecha']))?>
          </td>
        </tr>
      </tbody>
    </table>

    <div class="space-block"></div>

    <table class="table bordered" cellspacing="0">
      <tbody>
        <tr>
          <td class="text-left" style="width:72mm">
            <b>Codigo:</b> <?=$detalleVenta['id']?> <br>
            <b>Cliente:</b> <?=$cliente?$cliente['nombre']:''?> <br>
            <b>Nombre comercial:</b> <?=$cliente?$cliente['nombre_comercial']:''?> <br>
            <b>Contacto:</b> <?=$cliente?$cliente['contacto']:''?> <br>
          </td>
          <td style="width:72mm">
            <b>Teléfono:</b> <?=$cliente?$cliente['telefono']:''?> <br>
            <b>Dirección:</b> <?=$cliente?$cliente['direccion']:''?> <br>
            <b>NIT Cliente:</b> <?=$cliente?$cliente['nitci']:''?>
          </td>

          <td class="text-left" style="width:72mm">
            <!-- <b>Numero:</b> -->
            <b>Fecha:</b> <?=date('d-m-Y', strtotime($detalleVenta['Fecha']))?> <br>
            <!-- <b>Fecha Vto:</b> <?=date('d-m-Y', strtotime($detalleVenta['Fecha']))?> <br> -->
            <b>Factura:</b> <?=$detalleVenta['factura']?> <br>
            <b>Forma de pago:</b> <?=$detalleVenta['cobro_credito']==0?'Al contado':'A credito' ?> <br>
          </td>
        </tr>
      </tbody>
    </table>

    <div class="space-block"></div>

    <table class="table bordered" cellspacing="0">
      <tbody>
        <tr>
          <td class="text-center" style="width:36mm;height:18mm; font-size:15px; border-right:1px solid black;">
            Total: <?=$monedaString?><?=number_format($detalleVenta['Total'],2)?>
          </td>
          <td class="text-center" style="width:36mm;height:18mm; font-size:15px; border-right:1px solid black;">
            Descuento: <?=$monedaString?><?=number_format($detalleVenta['descuento'],2)?>
          </td>
          <td class="text-center" style="width:36mm;height:18mm; font-size:15px; border-right:1px solid black;">
            Total neto: <?=$monedaString?><?=number_format($detalleVenta['ventaTotalNeto'],2)?>
          </td>
          <td class="text-center" style="width:36mm;height:18mm; font-size:15px; border-right:1px solid black;">
            pagado: <?=$monedaString?><?=number_format($detalleVenta['pagado'],2)?>
          </td>
          <td class="text-center" style="width:36mm;height:18mm; font-size:15px; border-right:1px solid black;">
            Por cobrar: <?=$monedaString?><?=number_format($detalleVenta['porCobrar'],2)?>
          </td>
          <td class="text-center" style="width:36mm;height:18mm; font-size:15px; border-right:1px solid black;">
          </td>
        </tr>
      </tbody>
    </table>

  </body>
</html>