<!-- ******************************************************************************************************************* -->
<!-- ******************************************************************************************************************* -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Administrar Roles
      <small>&nbsp;</small>
    </h1>
    <!-- <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol> -->

</section>

<!-- Main content -->
<section class="content">
  <div id="msj"></div>
  <!-- ========================================================================================================================= -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Lista de Roles</h3>
          <!-- ****************************** -->
          <div class="pull-right box-tools">
            <button type="button" class="btn btn-primary btn-sm" id="btnNuevoRol" data-toggle="tooltip" title="Agregar nuevo Rol" data-original-title="Agregar nuevo rol">
              <i class="fa fa-user-secret" aria-hidden="true"></i> Nuevo Rol
            </button>
          </div>
          <!-- ****************************** -->
        </div>
        <div class="box-body">
          <div class="col-lg-12 col-xs-12">
            <!-- ñññññññññññññññññññññññññññññññññññññññññññññññññññññññññññññññññññññññññññññññññññññññññññññ           -->
            <div class="table-responsive">
              <!-- <table cellspacing="0" cellpadding="0" id="tbInventario" class="table table-striped table-bordered table-hover table-condensed"> -->
              <table id="tbRoles" class="table table-striped table-bordered dt-responsive nowrap table-hover table-condensed" cellspacing="0" width="100%" style="background: white!important">
                <thead>
                  <tr>
                    <th class="bg-primary text-capitalize"><span>Id</span></th>
                    <th class="bg-primary text-capitalize"><span>Rol</span></th>
                    <th class="bg-primary text-capitalize"><span>Acciones</span></th>

                  </tr>
                </thead>
                <tbody>
                  <?php
                  foreach ($roles as $rol) {
                    ?>
                    <tr data-id="<?=$rol['id']?>">
                    <td class="text-center"><i class="fa fa-user-secret" aria-hidden="true"></i></td>
                      <td class="text-left text-uppercase"><?=$rol['rol']?></td>
                      <td class="text-center">
                        <a class="btn btn-primary btn-xs editarr" data-idp="<?=$rol['id']?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                        <a href="#" class="btn btn-danger btn-xs delp" title="Eliminar" data-idp="<?=$rol['id']?>"><i class="fa fa-trash" aria-hidden="true"></i></a>
                      </td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
              <!-- ñññññññññññññññññññññññññññññññññññññññññññññññññññññññññññññññññññññññññññññññññññññññññññññ           -->
            </div>
          </div>
        </div>
        <!-- ========================================================================================================================= -->

        <!-- Your Page Content Here -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- ******************************************************************************************************************* -->
    <!-- ******************************************************************************************************************* -->
