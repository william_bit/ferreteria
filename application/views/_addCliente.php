<form class='form text-capitalize' id='<?=$idForm?>' autocomplete='off'>
  <input type='hidden' name='idc' value='<?=$idCliente?>'/>
    <div class='form-group'>
      <label>Nombre(s) del cliente</label>
      <input type='text' class='form-control' placeholder='Nombre' name='nombre' maxlength='50' required='required' value='<?=$nombre?>'/>
    </div>

    <div class='form-group'>
      <label>Apellido</label>
      <input type='text' class='form-control' placeholder='Apellido Materno' name='apellido' maxlength='20' required='required' value='<?=$cliente['apellido']?>'/>
    </div>

    <div class='form-group'>
      <label>Nombre Comercial</label>
      <input type='text' class='form-control' placeholder='Nombre Comercial' name='nombre_comercial' maxlength='50' required='required' value='<?=$nombre_comercial?>'/>
    </div>

    <div class='form-group'>
      <label>Transporte</label>
      <input type='text' class='form-control' placeholder='Transporte' name='transporte' maxlength='50' required='required' value='<?=$transporte?>'/>
    </div>

    <div class='form-group'>
      <label>Direccion</label>
      <input type='text' class='form-control' placeholder='Calle' name='direccion' maxlength='30' value='<?=$cliente['direccion']?>'/>
    </div>


    <div class='form-group'>
      <label>Nit / CI</label>
      <input type='text' class='form-control' placeholder='Colonia' name='nitci' maxlength='30'  value='<?=$cliente['nitci']?>'/>
    </div>
    
    <div class='form-group'>
      <label>codigo</label>
      <input type='text' class='form-control' placeholder='codigo' name='codigo' maxlength='30'  value='<?=$cliente['codigo']?>'/>
    </div>
    <div class='form-group'>
      <label>razon social</label>
      <input type='text' class='form-control' placeholder='razon_social' name='razon_social' maxlength='30'  value='<?=$cliente['razon_social']?>'/>
    </div>
    <div class='form-group'>
      <label>contacto</label>
      <input type='text' class='form-control' placeholder='contacto' name='contacto' maxlength='30'  value='<?=$cliente['contacto']?>'/>
    </div>
    <div class='form-group'>
      <label>email</label>
      <input type='email' class='form-control' placeholder='email' name='email' maxlength='30'  value='<?=$cliente['email']?>'/>
    </div>
    <div class='form-group'>
      <label>telefono</label>
      <input type='number' min="0" step="1" class='form-control' placeholder='telefono' name='telefono' maxlength='30'  value='<?=$cliente['telefono']?>'/>
    </div>
    <div class='form-group'>
      <label>ciudad</label>
      <input type='text' class='form-control' placeholder='ciudad' name='ciudad' maxlength='30'  value='<?=$cliente['ciudad']?>'/>
    </div>

  <br/><br/>
  <button type='submit' class='btn btn-success pull-right'><?=$textoBoton?></button>
  <div class='pull-right'>&nbsp;&nbsp;&nbsp;&nbsp;</div>
  <button type='button' class='btn btn-default pull-right' data-dismiss='modal'>Cancelar</button>
</form>
