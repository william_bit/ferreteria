<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pventa extends CI_Controller
{
 
  public function __construct()
  {
    parent::__construct();
  }


  public function getItem()
  {
    //el codigo que se recibe en realidad es el modelo del producto
    $codigo = $this->input->post('codigo');
    // $item = $this->consultas->getInventario($codigo);
    $item = $this->consultas->getInventarioWithModel($codigo);
    echo json_encode($item);
  }


  public function Importe()
  {
    $codigo = $this->input->post('codigo');
    $cantidad = $this->input->post('cantidad');

    $item = $this->consultas->getInventario($codigo);
    if($cantidad<$item['cantidadMayoreo'])
    {
      $total = $cantidad * $item['precio'];
    }
    else {
      $total = $cantidad * $item['precioMayoreo'];
    }
    echo $total;
  }

  public function NuevaVenta()
  {
    $idUser = $this->session->userdata('idUser');
    // // obtener el ultimo id de ventas del mismo usuario
    $maxIdVentas = $this->consultas->getMaxIdVentasByUser($idUser);
    $dataVenta = array(
      'idUsuario' => $idUser,
    );
    if($maxIdVentas!=0)
    {
      if($maxIdVentas['Total']<=0)
      {
        $this->delete->delMovimientosFallidos($maxIdVentas['id']);
      }
      if($maxIdVentas['Total']<=0)
      {
        echo $maxIdVentas['id'];
        die();
      }
      else{
        $idVenta = $this->insertar->newVenta($dataVenta);
        echo $idVenta;
        die();
      }
    }

    // asignar nueva venta a este usuario
    $idVenta = $this->insertar->newVenta($dataVenta);
    echo $idVenta;
  }

  public function delItemToVenta()
  {
    $idVenta=$this->input->post('idVenta');
    $codigoItem=$this->input->post('codigo');
    $item = $this->consultas->getInventario($codigoItem);
    $this->delete->delMovimiento($idVenta,$item['id']);
    echo "1";
  }

  public function v_recibido()
  {
    $total=$this->input->post('total');
    $idVenta=$this->input->post('idVenta');
    $impuesto=$this->input->post('impuesto');
    $descuento=$this->input->post('descuento');
    $data = array(
      'total'=>$total,
      'idVenta'=>$idVenta,
      'impuesto'=>$impuesto,
      'descuento'=>$descuento
    );
    if($impuesto){
      $configs = $this->consultas->getConfigs();
      $data['impuestoPorciento'] = $configs->impuestoPorciento;
      $data['nombreImpuesto'] = $configs->nombreImpuesto;
    }
    $data['monedaString'] = $this->consultas->getMonedaString();

    $this->load->view('_cobrar',$data);
  }

  /**
   * renderiza el formulario paraabrir 
   * el nuevo des cuento y controla que el 
   * importe segun el rpeio unitario o mayorista 
   * que bieno por la cantidad el producto
   */
  public function addDescuento(){
    $idVenta = $this->input->post('idVenta');
    $idItem = $this->input->post('idInventario');
    $movimiento = $this->consultas->getMovimientoVenta($idVenta,$idItem);


    $resultadoItem = $this->consultas->getInventariobyId($idItem);

    $precio = $resultadoItem['precio'];
    $importe = 0;

    if($movimiento['cantidad'] < $resultadoItem['cantidadMayoreo']){
      $importe = $movimiento['cantidad']*floatval($resultadoItem['precio']);
    }
    else{
    $importe = $movimiento['cantidad']*floatval($resultadoItem['precioMayoreo']);
      $precio = $resultadoItem['precioMayoreo'];
    }

    $data = array(
      'idMovimientoInventario' => $movimiento['id'],
      // 'costoUnitario' => $movimiento['costoUnitario'],
      // 'cantidad' => $movimiento['cantidad'],
      'descuento' => $movimiento['descuento'],
      'idItem' => $idItem,
      'idVenta' => $idVenta,
      "precio"=> $precio,
      "importe"=> $importe,
    );
    $data['monedaString']=$this->consultas->getMonedaString();
    $this->load->view('_descuento',$data);
  }

  /**
   * guarda el dato de descuento modificado y 
   * muestra el resultado con los datos que se
   * necesitan para volver a cargar la grilla
   */
  public function guardardescuento(){
    $dataMovimiento = array(
      'descuento'=>$this->input->post('descuento')
    );
    $idMovimiento = $this->input->post('idMovimientoInventario');
    $whereMovimiento = array(
      'id'=>$idMovimiento
    );
    
    $res = $this->insertar->updateMovimientoVenta($dataMovimiento,$whereMovimiento);

    $resultadoItem = $this->consultas->getInventariobyId($this->input->post('idItem'));

    $movimientoVenta = $this->consultas->getMovimientoVenta($this->input->post('idVenta'), $this->input->post('idItem'));

    $importe = $this->input->post("importe");
    $descuento = floatval($movimientoVenta['descuento']);
    $porDescuento = ($importe * $descuento) / 100;
    $importeNeto = $importe - $porDescuento;

    $dataRes = array(
      "resultadoItem"=> $resultadoItem,
      "precio"=> $this->input->post("precio"),
      "monedaString"=>$this->consultas->getMonedaString(),
      "cantidad"=> $movimientoVenta['cantidad'],
      "importe"=> $importe,
      "descuento"=> $movimientoVenta['descuento'],
      "importeNeto"=> $importeNeto,
      "movimiento"=> $movimientoVenta
    );

    if ( $res ){
      echo json_encode( $dataRes );
    }else{
      echo json_encode( false );
    }
  }

  /**
   * modifica el etributo descuento de 
   * lata bla movimiento venta
   */
  public function setFactura(){
    $idVenta = $this->input->post('idVenta');
    $idItem = $this->input->post('idInventario');
    $movimiento = $this->consultas->getMovimientoVenta($idVenta,$idItem);

    $dataMovimiento = array(
      'factura'=>$this->input->post('factura')
    );
    $idMovimiento = $movimiento['id'];
    $whereMovimiento = array(
      'id'=>$idMovimiento
    );
    
    $res = $this->insertar->updateMovimientoVenta($dataMovimiento,$whereMovimiento);
    echo json_encode($res);
  }

  public function concretarVenta()
  {
    $total=$this->input->post('total');
    $idVenta=$this->input->post('idVenta');
    $recibido=$this->input->post('recibido');
    $impuesto=$this->input->post('impuesto');
    $descuento=$this->input->post('descuento');
    $cobroCredito=$this->input->post('cobro_credito') == 'on'?1:0;
    $idCliente = $this->input->post('idcliente');

    $impuestoPorciento=0;
    $nombreImpuesto="";
    $cambio = $recibido-$total;
    $datos=array(
      'total' => $total,
      'idVenta' => $idVenta,
      'recibido' => $recibido,
      'cambio' => $cambio,
      'descuento' => $descuento,
      'cobro_credito' => $cobroCredito,
    );

    //si tiene un impuesto
    if($impuesto){
      $impuestoPorciento=$this->input->post('impuestoPorciento');
      $nombreImpuesto=$this->input->post('nombreImpuesto');
    }

    if (!isset($idCliente)){
      echo json_encode($datos);
      die();
    }
    
    // si lo recibido no es igual al total entonces
    if($cambio<0 && $cobroCredito == 0)
    {
      echo json_encode($datos);
      die();
    }

    //recording venta
    $data = array(
      'Total'=>$total,
      'Fecha'=>date("Y-m-d h:i:s"),
      'idCliente'=>$idCliente,
      'factura'=>$this->input->post('factura'),
      'descuento'=>$this->input->post('descuento'),
      'impuestoPorciento'=>$impuestoPorciento,
      'nombreImpuesto'=>$nombreImpuesto,
      'cobro_credito'=>$cobroCredito,
      'estado'=>$cobroCredito==1?0:1,
    );
    $where=$idVenta;
    $this->insertar->setVenta($data,$where);

    //descontar productos del inventario
    $listaItemsVenta = $this->consultas->getItemsDeVentas($idVenta);
    foreach ($listaItemsVenta as $itemVenta) {
      $myItem=$this->consultas->getInventariobyId($itemVenta['idInventario']);
      $nuevaCantidad = $myItem['cantidad']-$itemVenta['cantidad'];
      $dataSetItem=array(
        'cantidad'=>$nuevaCantidad
      );
      $whereSetItem=array(
        'id'=>$itemVenta['idInventario']
      );
      $this->insertar->setProducto($dataSetItem,$whereSetItem);
    }

    //registrar el pago de la venta
    $data=array(
      'monto'=>$recibido,
      'idVenta'=>$idVenta,
      'fecha'=>date('Y-m-d')
    );
    $this->insertar->insertPagoVenta($data);

    echo json_encode($datos);
  }





  public function verificarProducto()
  {
    //el codigo que se recibe en realidad es el modelo del producto
    $codigo = $this->input->post('codigo');
    // echo $this->consultas->comprobarCodigo($codigo);
    //retorna true or false si el producto existe segun el modelo del producto
    echo $this->consultas->comprobarModelo($codigo);
  }





  public function addItemToVenta()
  {
    $idVenta = $this->input->post('idVenta');
    $idItem = $this->input->post('idItem');
    $cantidad = $this->input->post('cantidad');

    $item = $this->consultas->getInventariobyId($idItem);
    if(!$item){
      $resultado = array(
        'validante'=>'n',
      );
      echo json_encode($resultado);
      die();
    }

    //verificando si la cantidad supera el indice de mayoreo para actualizar el precio
    $costo=$item['precio'];
    if($cantidad>=$item['cantidadMayoreo']){
      $costo=$item['precioMayoreo'];
    }
    //comprobar si hay mas de este articulo para nadmas sumarlo
    $exysteItemEnVenta = $this->consultas->comprobarItemEnVenta($idVenta,$idItem);
    if($exysteItemEnVenta)
    {
      $movimiento = $this->consultas->getMovimientoVenta($idVenta,$idItem);
      $item = $this->consultas->getInventariobyId($idItem);
      $sumCantidad = $cantidad + $movimiento['cantidad'];
      //verificando si el stock alcanza para realizar la venta
      if ($sumCantidad <= $item['cantidad']){
        $dataMovimiento = array(
          'cantidad'=>$sumCantidad,
          'costoUnitario'=>$costo
        );
        $whereMovimiento = array(
          'id'=>$movimiento['id']
        );
        $this->insertar->updateMovimientoVenta($dataMovimiento,$whereMovimiento);
        $resultado = array(
          'validante'=>'r',//el producto ya ha sido agregado
          'ncantidad'=>$sumCantidad,
          'dataMovimiento'=>$movimiento
        );
        echo json_encode($resultado);
      }else{
        $resultado = array(
          'validante'=>'n',
        );
        echo json_encode($resultado);
        die();
      }
    }
    else {
      //verificando si el stock alcanza para realizar la venta
      if ($cantidad <= $item['cantidad']){
        $dataMovimiento = array(
          'idVenta' => $idVenta,
          'idInventario'=>$idItem,
          'cantidad'=>$cantidad,
          'costoUnitario'=>$costo,
          'descuento'=> 0
        );
        $this->insertar->newMovimientoVenta($dataMovimiento);
        $resultado = array(
          'validante'=>'x'//el produco no se a agregado antes
        );
        echo json_encode($resultado);
      }else{
        $resultado = array(
          'validante'=>'n',
        );
        echo json_encode($resultado);
        die();
      }

    }
  }


  /**
   * imprime el comprobante de venta
   *
   * @return void
   */
  public function impVenta()
  {
    $idVenta = $this->input->post('idVenta');
    $detalleItem = $this->consultas->getItemsDeVentas($idVenta);
    $detalleVenta = $this->consultas->getVentaById($idVenta);
    $nombreEmpresa = $this->consultas->getConfigs()->nombreEmpresa;
    $vendedor=$this->consultas->getUsers($detalleVenta['idUsuario']);
    $cliente=$this->consultas->getClientes($detalleVenta['idCliente']);
    //total neto total menos el descuento
    $ventaTot = floatval($detalleVenta['Total']);
    $ventaPorTot = ( $ventaTot * floatval($detalleVenta['descuento']) ) / 100;
    $detalleVenta['ventaTotalNeto'] = $ventaTot - $ventaPorTot;
    //agregando lo objetos de producto a el array de items de venta
    for ($i=0; $i < count($detalleItem); $i++) { 
      $producto = $this->consultas->getInventariobyId($detalleItem[$i]['idInventario']);
      $tipo = $this->consultas->getTipo($producto['idTipo']);
      $producto['objTipo'] = $tipo;
      $detalleItem[$i]['producto'] = $producto;

      $puProducto = floatval($detalleItem[$i]['producto']['precio']);
      $porTot = ( $puProducto * floatval($detalleItem[$i]['descuento']) ) / 100;
      $detalleItem[$i]['puTotal'] = $puProducto - $porTot;

      $puProducto = floatval($detalleItem[$i]['costoUnitario']);
      $cantidad = floatval($detalleItem[$i]['cantidad']);
      $precioNeto = $puProducto * $cantidad;
      $porTotNeto = ( $precioNeto * floatval($detalleItem[$i]['descuento']) ) / 100;
      $detalleItem[$i]['importeNeto'] = $precioNeto - $porTotNeto;
    }
    //verificando si la venta tiene un cliente o no
    if ( isset($cliente['nombre']) == false){
      $cliente = null;
    }
    //convirtiendo numeros a palabras
    $this->load->library('numbertowords');
    // $a=$this->load->library('numbertowords');
    // $totalLiteral = $this->numbertowords->convert_number($detalleVenta['ventaTotalNeto']);
    $moneda=$this->consultas->getMonedaString();
    $totalLiteral = $this->numbertowords->convertir($detalleVenta['ventaTotalNeto'], $moneda, 'Centavos', true );

    $data=array(
      'detalle'=>$detalleItem,
      'detalleVenta'=>$detalleVenta,
      'vendedor'=>$vendedor,
      'cliente'=>$cliente,
      'nombreEmpresa'=>$nombreEmpresa,
      'totalLiteral'=>$totalLiteral
    );
    $data['monedaString']=$this->consultas->getMonedaString();
    $this->load->view('inventario/_ticket',$data);
  }




  public function buscarpr()
  {
    $inventario = $this->consultas->getInventario();
    $data = array('inventario' => $inventario);
    $data['monedaString'] = $this->consultas->getMonedaString();
    $this->load->view('_busqueda',$data);
  }







  // /**
  //  * ventas realizadas a credito
  //  */
  // public function cobrarventa()
  // {
  //   $idUser = $this->session->userdata('idUser');
  //   // obtener la lisa de ventas realizadas a credito
  //   $ventas = $this->consultas->getVentasACredito();
  //   $data = array(
  //     'ventas'=>$ventas,
  //   );
  //   $data['monedaString'] = $this->consultas->getMonedaString();

  //   $dataHeader['titulo']="Admin";
  //   $this->load->view('header',$dataHeader);

  //   $dataSidebar = array();
  //   $dataSidebar['classInventario']="";
  //   $dataSidebar['classVentas']="";
  //   $dataSidebar['classMovimientos']="";
  //   $dataSidebar['classUsuarios']="";
  //   $dataSidebar['classCreditos']="";

  //   $dataSidebar['classInventarioGeneral']="active";
  //   $dataSidebar['classInventarioModificar']="";
  //   $dataSidebar['classInventarioAgregar']="";
  //   $dataSidebar['classInventarioNuevo']="";
  //   $dataSidebar['classInventarioCBarras']="";
  //   $dataSidebar['classConfiguraciones']="";
  //   $dataSidebar['classProveedores']="";
  //   $dataSidebar['classClientes']="";

  //   $tema = $this->consultas->configTema();
  //   $dataSidebar['tema']="$tema";
  //   $dataSidebar['usuario']=$user;
  //   $this->load->view('sidebar',$dataSidebar);

  //   $this->load->view('cobrar_ventas',$data);
  //   $this->load->view('main-footer');
  //   $dataFooter=array(
  //     'scripts'=> "<script src='".base_url()."js/cobrarVentas.js'></script>"
  //   );

  //   $this->load->view('footer',$dataFooter);
  // }
}
?>
