<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Clientes extends CI_Controller
{
    
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('rol') >= 3) {
            redirect(base_url());
        }
    }
    
    public function index()
    {
        $idUser = $this->session->userdata('idUser');
        $rol  = $this->session->userdata('rol');
        $user = $this->consultas->getUsers($idUser);
        $data = array();
        $dataHeader['titulo'] = "Usuarios";
        $dataSidebar = array();
        $dataSidebar['classInventario'] = "";
        $dataSidebar['classVentas'] = "";
        $dataSidebar['classMovimientos'] = "";
        $dataSidebar['classUsuarios'] = "";
        $dataSidebar['classCreditos'] = "";
        
        $dataSidebar['classInventarioGeneral'] = "";
        $dataSidebar['classInventarioModificar'] = "";
        $dataSidebar['classInventarioAgregar'] = "";
        $dataSidebar['classInventarioNuevo'] = "";
        $dataSidebar['classInventarioCBarras'] = "";
        $dataSidebar['classConfiguraciones'] = "";
        $dataSidebar['classProveedores'] = "";
        $dataSidebar['classClientes'] = "active";
        $dataSidebar['classRoles']="";
        $dataSidebar['usuario'] = $user;
        $tema = $this->consultas->configTema();
        $dataSidebar['tema'] = "$tema";
        
        $data['clientes'] = $this->consultas->getClientes();
        $this->load->view('header', $dataHeader);
        $this->load->view('sidebar', $dataSidebar);
        $this->load->view('clientes', $data);
        $this->load->view('main-footer');
        $dataFooter = array(
            'scripts' => "<script src='" . base_url() . "js/clientes.js'></script>"
        );
        $dataFooter['scripts'] .= "<script src='" . base_url() . "js/tema.js'></script>";
        $this->load->view('footer', $dataFooter);
    }
    
    public function addHtml($idCliente = '0')
    {
        $codigo = "";
        $nombre     = "";
        $nombre_comercial     = "";
        $transporte     = "";
        $textoBoton = "Agregar";
        $idForm     = "formAddCliente";
        $apellido = "";
        $direccion = "";
        $nitci = "";

        $razon_social = "";
        $contacto = "";
        $email = "";
        $telefono = "";
        $ciudad = "";

        $myCliente  = array(
            'id' => '',
            'codigo' => '',
            'nombre' => '',
            'nombre_comercial' => '',
            'transporte' => '',
            'apellido' => '',
            'direccion' => '',
            'nitci' => '',
            'razon_social' => '',
            'contacto' => '',
            'email' => '',
            'telefono' => '',
            'ciudad' => '',
        );
        
        if ($idCliente != 0) {
            $myCliente  = $this->consultas->getClientes($idCliente);
            $codigo     = $myCliente['codigo'];
            $nombre     = $myCliente['nombre'];
            $nombre_comercial     = $myCliente['nombre_comercial'];
            $transporte     = $myCliente['transporte'];
            $apellido     = $myCliente['apellido'];
            $nitci     = $myCliente['nitci'];
            $direccion     = $myCliente['direccion'];

            $razon_social     = $myCliente['razon_social'];
            $contacto     = $myCliente['contacto'];
            $email     = $myCliente['email'];
            $telefono     = $myCliente['telefono'];
            $ciudad     = $myCliente['ciudad'];

            $textoBoton = "Modificar";
            $idForm     = "formModCliente";
        }
        
        $data = array(
            'idForm' => $idForm,
            'idCliente' => $idCliente,
            'codigo' => $codigo,
            'nombre' => $nombre,
            'nombre_comercial' => $nombre_comercial,
            'transporte' => $transporte,
            'apellido' => $apellido,
            'nitci' => $nitci,
            'direccion' => $direccion,
            'razon_social' => $razon_social,
            'contacto' => $contacto,
            'email' => $email,
            'telefono' => $telefono,
            'ciudad' => $ciudad,
            'textoBoton' => $textoBoton,
            'roles' => $this->consultas->getRoles(),
            'cliente' => $myCliente
        );
        $this->load->view('_addCliente', $data);
    }
    
    public function addCliente()
    {
        $datos = array(
            'codigo' => $this->input->post('codigo'),
            'nombre' => $this->input->post('nombre'),
            'nombre_comercial' => $this->input->post('nombre_comercial'),
            'transporte' => $this->input->post('transporte'),
            'apellido' => $this->input->post('apellido'),
            'direccion' => $this->input->post('direccion'),
            'nitci' => $this->input->post('nitci'),
            'razon_social' => $this->input->post('razon_social'),
            'contacto' => $this->input->post('contacto'),
            'email' => $this->input->post('email'),
            'telefono' => $this->input->post('telefono'),
            'ciudad' => $this->input->post('ciudad'),
        );
        $this->insertar->newCliente($datos);
    }
    
    
    public function modCliente()
    {
        $idCliente = $this->input->post('idc');
        $datos     = array(
            'codigo' => $this->input->post('codigo'),
            'nombre' => $this->input->post('nombre'),
            'nombre_comercial' => $this->input->post('nombre_comercial'),
            'transporte' => $this->input->post('transporte'),
            'apellido' => $this->input->post('apellido'),
            'direccion' => $this->input->post('direccion'),
            'nitci' => $this->input->post('nitci'),
            'razon_social' => $this->input->post('razon_social'),
            'contacto' => $this->input->post('contacto'),
            'email' => $this->input->post('email'),
            'telefono' => $this->input->post('telefono'),
            'ciudad' => $this->input->post('ciudad'),
        );
        $where     = array(
            'id' => $idCliente
        );
        $this->insertar->setCliente($datos, $where);
    }
    
    public function delCliente()
    {
        $idCliente = $this->input->post('idc');
        $this->delete->delCliente($idCliente);
    }

    public function showajaxclientes(){
      header('Content-Type: application/json');
      $datos = $this->consultas->getClientesFilter($this->input->post('nombre'), $this->input->post('apellido'), $this->input->post('direccion'), $this->input->post('nitci'));
      echo json_encode($datos);
    }
}
?>