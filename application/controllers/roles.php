<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Roles extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    if($this->session->userdata('rol')!=1){
      redirect(base_url());
    }
  }

  public function index()
  {
    $idUser = $this->session->userdata('idUser');
    $rol= $this->session->userdata('rol');
    $user=$this->consultas->getUsers($idUser);
    $data = array();
    $dataHeader['titulo']="Roles";
    $dataSidebar = array();
    $dataSidebar['classInventario']="";
    $dataSidebar['classVentas']="";
    $dataSidebar['classMovimientos']="";
    $dataSidebar['classUsuarios']="";
    $dataSidebar['classCreditos']="";

    $dataSidebar['classInventarioGeneral']="";
    $dataSidebar['classInventarioModificar']="";
    $dataSidebar['classInventarioAgregar']="";
    $dataSidebar['classInventarioNuevo']="";
    $dataSidebar['classInventarioCBarras']="";
    $dataSidebar['classConfiguraciones']="";
    $dataSidebar['classProveedores']="";
    $dataSidebar['classClientes']="";
    $dataSidebar['classRoles']="active";
    $dataSidebar['usuario']=$user;
    $tema = $this->consultas->configTema();
    $dataSidebar['tema']="$tema";

    $data['roles']=$this->consultas->getRoles();
    $this->load->view('header',$dataHeader);
    $this->load->view('sidebar',$dataSidebar);
    $this->load->view('roles',$data);
    $this->load->view('main-footer');
    $dataFooter=array(
      'scripts'=> "<script src='".base_url()."js/roles.js'></script>"
    );
    $dataFooter['scripts'].="<script src='".base_url()."js/tema.js'></script>";
    $this->load->view('footer',$dataFooter);
  }

  public function addHtml($idRol='0')
  {
    $rol="";
    $textoBoton="Agregar";
    $idForm="formAddRol";

    // $rfc="";
    // $telefono="";
    // $telefono2="";
    // $telefono3="";
    // $telefono4="";
    // $codigo="";
    // $apellidos="";
    // $razon_social="";
    // $celular="";
    // $email="";
    // $direccion="";
    // $pais="";
    // $ciudad="";
    // $nit="";
    // $observacion="";

    $myRol =array(
      'rol'=>'',
      // 'rfc'=>'',
      // 'telefono'=>'',
      // 'telefono2'=>'',
      // 'telefono3'=>'',
      // 'telefono4'=>'',
      // 'codigo'=>'',
      // 'apellidos'=>'',
      // 'razon_social'=>'',
      // 'celular'=>'',
      // 'email'=>'',
      // 'direccion'=>'',
      // 'pais'=>'',
      // 'ciudad'=>'',
      // 'nit'=>'',
      // 'observacion'=>'',
    );

    if($idRol!=0){
      $myRol = $this->consultas->getRoles($idRol);
      $rol=$myRol['rol'];
      // $rfc=$myProv['rfc'];
      // $telefono=$myProv['telefono'];
      // $telefono2=$myProv['telefono2'];
      // $telefono3=$myProv['telefono3'];
      // $telefono4=$myProv['telefono4'];
      // $codigo=$myProv['codigo'];
      // $apellidos=$myProv['apellidos'];
      // $razon_social=$myProv['razon_social'];
      // $celular=$myProv['celular'];
      // $email=$myProv['email'];
      // $direccion=$myProv['direccion'];
      // $pais=$myProv['pais'];
      // $ciudad=$myProv['ciudad'];
      // $nit=$myProv['nit'];
      // $observacion=$myProv['observacion'];

      $textoBoton="Modificar";
      $idForm="formModProv";
    }

    $data=array(
      'idForm'=>$idForm,
      'idRol'=>$idRol,
      'rol'=>$rol,
      // 'rfc'=>$rfc,
      // 'telefono'=>$telefono,
      // 'telefono2'=>$telefono2,
      // 'telefono3'=>$telefono3,
      // 'telefono4'=>$telefono4,
      // 'codigo'=>$codigo,
      // 'apellidos'=>$apellidos,
      // 'razon_social'=>$razon_social,
      // 'celular'=>$celular,
      // 'email'=>$email,
      // 'direccion'=>$direccion,
      // 'pais'=>$pais,
      // 'ciudad'=>$ciudad,
      // 'nit'=>$nit,
      // 'observacion'=>$observacion,
      'roles'=>$myRol,
      'textoBoton'=>$textoBoton,
    );
    $this->load->view('_addRol',$data);
  }

  public function addRol()
  {
    $datos = array(
      'rol'=>strtoupper($this->input->post('rol')),
      // 'rfc'=>strtoupper($this->input->post('rfc')),
      // 'telefono'=>$this->input->post('tel'),
      // 'telefono2'=>$this->input->post('tel2'),
      // 'telefono3'=>$this->input->post('tel3'),
      // 'telefono4'=>$this->input->post('tel4'),
      // 'codigo'=>$this->input->post('codigo'),
      // 'apellidos'=>$this->input->post('apellidos'),
      // 'razon_social'=>$this->input->post('razon_social'),
      // 'celular'=>$this->input->post('celular'),
      // 'email'=>$this->input->post('email'),
      // 'direccion'=>$this->input->post('direccion'),
      // 'pais'=>$this->input->post('pais'),
      // 'ciudad'=>$this->input->post('ciudad'),
      // 'nit'=>$this->input->post('nit'),
      // 'observacion'=>$this->input->post('observacion'),
    );
    // print_r($datos);
    $this->insertar->newRol($datos);
  }


  public function modRol()
  {
    $idRol = $this->input->post('idp');
    $datos = array(
      'rol'=>strtoupper($this->input->post('rol')),
      // 'rfc'=>strtoupper($this->input->post('rfc')),
      // 'telefono'=>$this->input->post('tel'),
      // 'telefono2'=>$this->input->post('tel2'),
      // 'telefono3'=>$this->input->post('tel3'),
      // 'telefono4'=>$this->input->post('tel4'),
      // 'codigo'=>$this->input->post('codigo'),
      // 'apellidos'=>$this->input->post('apellidos'),
      // 'razon_social'=>$this->input->post('razon_social'),
      // 'celular'=>$this->input->post('celular'),
      // 'email'=>$this->input->post('email'),
      // 'direccion'=>$this->input->post('direccion'),
      // 'pais'=>$this->input->post('pais'),
      // 'ciudad'=>$this->input->post('ciudad'),
      // 'nit'=>$this->input->post('nit'),
      // 'observacion'=>$this->input->post('observacion'),
    );
    $where = array(
      'id'=>$idRol
    );
    $this->insertar->setRol($datos,$where);
  }

  public function delRol()
  {
    $idProv = $this->input->post('idr');
    $this->delete->delRol($idProv);
  }





}
?>
