<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cobrarventas extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  /**
   * ventas realizadas a credito
   */
  public function index()
  {
    $idUser = $this->session->userdata('idUser');
    $rol= $this->session->userdata('rol');
    $user=$this->consultas->getUsers($idUser);
    if(!$idUser)
    {
      $dataHeader['titulo']="Login";
      $this->load->view('header',$dataHeader);
      $data['error'] ="";
      $this->load->view('login',$data);
      $dataFooter=array(
        'scripts'=> ""
      );
      $this->load->view('footer',$dataFooter);
      return false;
    }

    // obtener la lisa de ventas realizadas a credito
    $ventas = $this->consultas->getVentasACredito();
    $data = array(
      'ventas'=>$ventas,
    );
    $data['monedaString'] = $this->consultas->getMonedaString();

    $dataHeader['titulo']="Admin";
    $this->load->view('header',$dataHeader);

    $dataSidebar = array();
    $dataSidebar['classInventario']="";
    $dataSidebar['classVentas']="";
    $dataSidebar['classMovimientos']="";
    $dataSidebar['classUsuarios']="";
    $dataSidebar['classCreditos']="";

    $dataSidebar['classInventarioGeneral']="active";
    $dataSidebar['classInventarioModificar']="";
    $dataSidebar['classInventarioAgregar']="";
    $dataSidebar['classInventarioNuevo']="";
    $dataSidebar['classInventarioCBarras']="";
    $dataSidebar['classConfiguraciones']="";
    $dataSidebar['classProveedores']="";
    $dataSidebar['classClientes']="";

    $tema = $this->consultas->configTema();
    $dataSidebar['tema']="$tema";
    $dataSidebar['usuario']=$user;
    $this->load->view('sidebar',$dataSidebar);

    $this->load->view('cobrar_ventas',$data);
    $this->load->view('main-footer');
    $dataFooter=array(
      'scripts'=> "<script src='".base_url()."js/cobrarVentas.js'></script>"
    );

    $this->load->view('footer',$dataFooter);
  }

  /**
   * formulario para cobrar la venta
   * @return html
   */
  public function formcobrarventa()
  {
    $idVenta = $this->input->post('idVenta');

    $detalleVenta = $this->consultas->getVentaById($idVenta);
    $detalleVenta = $this->getVentaWithLogicAttr($detalleVenta);//updating logic attr to venta

    //flag auxiliar para desglear formulario
    $pagoTerminado = false;
    if ( $detalleVenta['porCobrar'] <= 0 ){
      $pagoTerminado = true;
    }
    //flag auxiliar para mostrar mensaje
    $pagado = $this->input->get('pagado');

    //verificando/obteniendo  cliente de la venta
    $cliente=$this->consultas->getClientes($detalleVenta['idCliente']);
    if ( isset($cliente['nombre']) == false){
      $cliente = null;
    }

    $data=array(
      'venta'=>$detalleVenta,
      'pagoTerminado'=>$pagoTerminado,
      'pagado'=>$pagado==1?true:false,
      'cliente'=>$cliente,
    );
    $data['monedaString']=$this->consultas->getMonedaString();
    $this->load->view('_form_cobrar_venta',$data);
  }

  /**
   * Registrando el pago de la 
   * venta pendiente
   * @return JSON
   */
  public function registrarpago(){
    $monto = $this->input->post('monto');
    $idVenta = $this->input->post('idVenta');

    $venta = $this->consultas->getVentaById($idVenta);
    $venta = $this->getVentaWithLogicAttr($venta);//updating logic attr to venta

    if ($monto <= 0 || $monto > $venta['porCobrar'] ){
      echo json_encode([
        'state'=>false,
        'message'=>'El monto debe ser mayor a 0 y menor a '.$venta['porCobrar']
      ]);
      die();
    }

    //registrar el pago de la venta
    $data=array(
      'monto'=>$monto,
      'idVenta'=>$idVenta,
      'fecha'=>date('Y-m-d')
    );
    $this->insertar->insertPagoVenta($data);

    //verificar si se termino de pagar la deuda
    $venta = $this->getVentaWithLogicAttr($venta);//updating logic attr to venta
    $pagoTerminado = false;
    if ( $venta['porCobrar'] <= 0 ){
      //actualizando venta
      $data = array(
        'estado'=>1,
      );
      $where=$idVenta;
      $this->insertar->setVenta($data,$where);
      $pagoTerminado = true;
    }

    echo json_encode([
      'state'=>true,
      'message'=>'El registro se realizo crrectamente',
    ]);
  }

  /**
   * imprime un comprobante de pago de la 
   * deuda de la venta a credito
   * @return html
   */
  public function impcomprobante(){
    $idVenta = $this->input->post('idVenta');
    $detalleItem = $this->consultas->getItemsDeVentas($idVenta);
    $detalleVenta = $this->consultas->getVentaById($idVenta);
    $nombreEmpresa = $this->consultas->getConfigs()->nombreEmpresa;
    $vendedor=$this->consultas->getUsers($detalleVenta['idUsuario']);
    $cliente=$this->consultas->getClientes($detalleVenta['idCliente']);

    //total neto total menos el descuento, atributos logicos extra
    $detalleVenta = $this->getVentaWithLogicAttr($detalleVenta);//updating logic attr to venta
    
    //verificando si la venta tiene un cliente o no
    if ( isset($cliente['nombre']) == false){
      $cliente = null;
    }

    $data=array(
      'detalleVenta'=>$detalleVenta,
      'vendedor'=>$vendedor,
      'cliente'=>$cliente,
      'nombreEmpresa'=>$nombreEmpresa,
    );
    $data['monedaString']=$this->consultas->getMonedaString();
    $this->load->view('_comp_cobro_venta',$data);
  }

  /**
   * obtiene  los atributos logicos extra
   * de una venta ventaTotalNeto, pagado y porCobrar
   * @param int $idVenta
   * @return array
   */
  private function getVentaWithLogicAttr($venta){
    $venta['pagado'] = $this->consultas->getTotalPagosVenta($venta['id']);
    $ventaTot = floatval($venta['Total']);
    $ventaPorTot = ( $ventaTot * floatval($venta['descuento']) ) / 100;
    $venta['ventaTotalNeto'] = $ventaTot - $ventaPorTot;
    $venta['porCobrar'] = floatval($venta['ventaTotalNeto']) - floatval($venta['pagado']);
    return $venta;
  }
}
?>
