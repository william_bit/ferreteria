<?php

class Consultas extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	public function consultasIniciales()
	{
		$sqlUsuarios = "SELECT id FROM tb_usuarios";
		$queryUsuarios = $this->db->query($sqlUsuarios);
		$usuarios=$queryUsuarios->num_rows();

		$sqlArticulos = "SELECT id FROM tb_inventario";
		$queryArticulos = $this->db->query($sqlArticulos);
		$articulos=$queryArticulos->num_rows();

		$hoy=date('Y-m-d');
		$sqlRecaudado = "SELECT sum(Total) as suma FROM tb_ventas WHERE Fecha >= '$hoy'";
		$queryRecaudado = $this->db->query($sqlRecaudado);
		$recaudado=$queryRecaudado->row();
		$recaudado=$recaudado->suma;
		$valores=array(
			'articulos'=>$articulos,
			'recaudado'=>$recaudado,
			'usuarios'=>$usuarios,
			'creditos'=>0
		);
		return $valores;
	}

	function isUser($user, $pass)
	{
		$sql = "SELECT * FROM tb_usuarios WHERE username ='$user'";
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			$sql = "SELECT * FROM tb_usuarios WHERE username ='$user' and pass = '$pass'";
			$query = $this->db->query($sql);
			if ($query->num_rows() > 0) {
				return '1';
			} else {
				return 'Contraseña incorrecta';
			}
		} else {
			return 'Usuario no encontrado';
		}
	}

	function findIdUser($user, $pass)
	{
		$sql = "SELECT * FROM tb_usuarios WHERE username ='$user' AND pass='$pass'";
		$query = $this->db->query($sql);
		return $query->row();
	}

	function existeUsername($user)
	{
		$sql = "SELECT id FROM tb_usuarios WHERE username ='$user'";
		$query = $this->db->query($sql);
		if ($query->num_rows() == 0) {
			return false;
		}
		return true;
	}



	function getConfigs()
	{
		$sql = "SELECT * FROM tb_config limit 1";
		$query = $this->db->query($sql);
		return $r=$query->row();
	}

	function configTema()
	{
		$sql = "SELECT tema FROM tb_config limit 1";
		$query = $this->db->query($sql);
		$r=$query->row();
		return $r->tema;
	}

	function configImpuesto()
	{
		$sql = "SELECT tema FROM tb_config limit 1";
		$query = $this->db->query($sql);
		$r=$query->row();
		return $r->tema;
	}

	function configNombreEmpresa()
	{
		$sql = "SELECT nombreEmpresa FROM tb_config limit 1";
		$query = $this->db->query($sql);
		$r=$query->row();
		return $r->nombreEmpresa;
	}
	function configLogo()
	{
		$sql = "SELECT logo FROM tb_config limit 1";
		$query = $this->db->query($sql);
		$r=$query->row();
		return $r->logo;
	}

	function getInventario($codigo="")
	{
		if($codigo!="")
		{
			$sql = "SELECT  inv.id,inv.codigo,inv.modelo,inv.master,inv.estado,inv.descripcion,inv.costo,inv.precio,inv.precioMayoreo,inv.cantidadMayoreo,inv.limiteStock,inv.idDepartamento,inv.cantidad,inv.idProveedor,inv.factura,dep.departamento,inv.idTipo,tv.nombreTipo, tp.nombre as proveedor
			FROM tb_inventario inv
			inner join tb_departamentos dep on inv.idDepartamento = dep.id
			inner join tb_tipos tv on inv.idTipo = tv.id
			inner join tb_proveedores tp on inv.idProveedor = tp.id
			WHERE inv.codigo = '$codigo'
			ORDER BY inv.descripcion ASC";
			$query = $this->db->query($sql);
			return $query->row_array();
		}
		else{
			$sql = "SELECT  inv.id,inv.modelo,inv.codigo,inv.master,inv.estado,inv.descripcion,inv.costo,inv.precio,inv.precioMayoreo,inv.cantidadMayoreo,inv.limiteStock,inv.idDepartamento,inv.cantidad,inv.idProveedor,inv.factura, dep.departamento,inv.idTipo,tv.nombreTipo, tp.nombre as proveedor
			FROM tb_inventario inv
			inner join tb_departamentos dep on inv.idDepartamento = dep.id
			inner join tb_tipos tv on inv.idTipo = tv.id
			inner join tb_proveedores tp on inv.idProveedor = tp.id
			ORDER BY inv.descripcion ASC";
			$query = $this->db->query($sql);
			return $query->result_array();
		}
	}
	
	/**
	 * busca el item inventario about model
	 *
	 * @param string $codigo
	 * @return void
	 */
	function getInventarioWithModel($codigo="")
	{
		if($codigo!="")
		{
			$sql = "SELECT  inv.id,inv.codigo,inv.modelo,inv.master,inv.estado,inv.descripcion,inv.costo,inv.precio,inv.precioMayoreo,inv.cantidadMayoreo,inv.limiteStock,inv.idDepartamento,inv.cantidad,inv.idProveedor,inv.factura,dep.departamento,inv.idTipo,tv.nombreTipo, tp.nombre as proveedor
			FROM tb_inventario inv
			inner join tb_departamentos dep on inv.idDepartamento = dep.id
			inner join tb_tipos tv on inv.idTipo = tv.id
			inner join tb_proveedores tp on inv.idProveedor = tp.id
			WHERE inv.modelo = '$codigo'
			ORDER BY inv.descripcion ASC";
			$query = $this->db->query($sql);
			return $query->row_array();
		}
		else{
			$sql = "SELECT  inv.id,inv.modelo,inv.codigo,inv.master,inv.estado,inv.descripcion,inv.costo,inv.precio,inv.precioMayoreo,inv.cantidadMayoreo,inv.limiteStock,inv.idDepartamento,inv.cantidad,inv.idProveedor,inv.factura, dep.departamento,inv.idTipo,tv.nombreTipo, tp.nombre as proveedor
			FROM tb_inventario inv
			inner join tb_departamentos dep on inv.idDepartamento = dep.id
			inner join tb_tipos tv on inv.idTipo = tv.id
			inner join tb_proveedores tp on inv.idProveedor = tp.id
			ORDER BY inv.descripcion ASC";
			$query = $this->db->query($sql);
			return $query->result_array();
		}
	}

	function getInventariobyId($id)
	{
		$sql = "SELECT *
		FROM tb_inventario
		WHERE id = '$id'";
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	
	function getInventarioAll()
	{
		$sql = "SELECT  *
		FROM tb_inventario";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	function getVentas()
	{
		$sql = "SELECT  vts.id,vts.idUsuario,vts.Total,vts.Fecha,u.nombre as nombreUsuario, vts.descuento
		FROM tb_ventas vts
		inner join tb_usuarios u on vts.idUsuario = u.id
		WHERE vts.Total>0";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function getItemsDeVentas($idVenta)
	{
		$sql = "SELECT * FROM  tb_movimientosventas	WHERE idVenta = $idVenta";
		$query = $this->db->query($sql);
		return $query->result_array();
	}



	function getDepartamentos($idDepartamento=0)
	{
		if($idDepartamento>0){
			$sql = "SELECT * FROM tb_departamentos WHERE id='$idDepartamento'";
			$query = $this->db->query($sql);
			return $query->row_array();
		}
		else {
			$sql = "SELECT * FROM tb_departamentos ORDER BY id ASC";
			$query = $this->db->query($sql);
			return $query->result_array();
		}
	}


	function getTipoVenta()
	{
		$sql = "SELECT * FROM tb_tipos ORDER BY id ASC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	function comprobarCodigo($codigo="")
	{
		$sql = "SELECT * FROM tb_inventario WHERE  codigo = '$codigo'";
		$query = $this->db->query($sql);
		if ($query->num_rows() == 0) {
			return false;
		}
		return true;
	}
	function comprobarModelo($modelo="")
	{
		$sql = "SELECT * FROM tb_inventario WHERE  modelo = '$modelo'";
		$query = $this->db->query($sql);
		if ($query->num_rows() == 0) {
			return false;
		}
		return true;
	}

	public function getUsers($id=0)
	{
		if($id!=0)
		{
			$sql = "SELECT u.*,r.rol
			FROM tb_usuarios u
			inner join tb_roles r on u.idRol = r.id
			Where u.id='$id'
			ORDER BY u.nombre ASC";
			$query = $this->db->query($sql);
			return $query->row_array();
		}
		$sql = "SELECT u.*,r.rol
		FROM tb_usuarios u
		inner join tb_roles r on u.idRol = r.id
		ORDER BY u.nombre ASC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function getClientes($id=0)
	{
		if($id!=0)
		{
			$sql = "SELECT * FROM tb_clientes Where id='$id'	ORDER BY nombre ASC";
			$query = $this->db->query($sql);
			return $query->row_array();
		}
		$sql = "SELECT * FROM tb_clientes ORDER BY nombre ASC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function getClientesFilter($paramNombre="", $paramApellido="", $paramDireccion="", $paramNitCi="")
	{
		$sql = "SELECT * FROM tb_clientes 
			WHERE nombre LIKE '$paramNombre%' 
			AND apellido LIKE '$paramApellido%'
			AND direccion LIKE '$paramDireccion%'
			AND nitci LIKE '".$paramNitCi."%'
			ORDER BY id DESC LIMIT 5";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function getProveedores($idProv=0){
		if($idProv>0){
			$sql = "SELECT * FROM tb_proveedores WHERE id='$idProv'	ORDER BY nombre ASC";
			$query = $this->db->query($sql);
			return $query->row_array();
		}
		else {
			$sql = "SELECT * FROM tb_proveedores	ORDER BY nombre ASC";
			$query = $this->db->query($sql);
			return $query->result_array();
		}
	}

	public function getRoles($idRol=0){
		if($idRol>0){
			$sql = "SELECT * FROM tb_roles WHERE id='$idRol'	ORDER BY rol ASC";
			$query = $this->db->query($sql);
			return $query->row_array();
		}
		else {
			$sql = "SELECT * FROM tb_roles	ORDER BY rol ASC";
			$query = $this->db->query($sql);
			return $query->result_array();
		}
	}


	// public function getRoles()
	// {
	// 	$sql = "SELECT * FROM tb_roles";
	// 	$query = $this->db->query($sql);
	// 	return $query->result_array();
	// }



	public function comprobarItemEnVenta($idVenta,$idItem)
	{
		$sql = "SELECT * FROM tb_movimientosventas WHERE idVenta = '$idVenta' AND idInventario='$idItem'";
		$query = $this->db->query($sql);
		if ($query->num_rows() == 0) {
			return false;
		}
		return true;
	}

	public function getMovimientoVenta($idVenta,$idItem)
	{
		$sql = "SELECT * FROM tb_movimientosventas WHERE idVenta = '$idVenta' AND idInventario='$idItem'";
		$query = $this->db->query($sql);
		return $query->row_array();
	}

	public function getMovimientosData($codigo="")
	{
		if($codigo==""){
			$sql = "SELECT mvs.id,mvs.idVenta,mvs.cantidad, vts.Fecha,inv.codigo,inv.descripcion,us.nombre as usuario,mvs.tipo,mvs.fechaEntrada
			FROM tb_movimientosventas mvs
			left outer join tb_ventas vts on mvs.idVenta=vts.id
			left outer join tb_inventario inv on mvs.idInventario=inv.id
			left outer join tb_usuarios us on vts.idUsuario=us.id";
			$query = $this->db->query($sql);
			return $query->result_array();
		}
		else{
			$sql = "SELECT mvs.id,mvs.idVenta,mvs.cantidad, vts.Fecha,inv.codigo,inv.descripcion,us.nombre as usuario,mvs.fechaEntrada
			FROM tb_movimientosventas mvs
			left outer join tb_ventas vts on mvs.idVenta=vts.id
			left outer join tb_inventario inv on mvs.idInventario=inv.id
			left outer join tb_usuarios us on vts.idUsuario=us.id
			WHERE inv.codigo=?";
			$query = $this->db->query($sql, array($codigo) );
			return $query->result_array();
		}
	}

	
	public function getVentaById($idVenta)
	{
		$sql = "SELECT * FROM tb_ventas WHERE id = '$idVenta'";
		$query = $this->db->query($sql);
		return $query->row_array();
	}

	function getMaxIdVentasByUser($idUsuario)
	{
		$sql = "SELECT * FROM tb_ventas WHERE idUsuario = '$idUsuario' ORDER BY id DESC limit 1";
		$query = $this->db->query($sql);
		if ($query->num_rows() == 0) {
			return 0;
		}
		else{
			return $query->row_array();
		}
	}



	public function recaudacionByUserHoy($idUser=0)
	{
		if($idUser!=0){
			$hoy=date('Y-m-d');
			$sqlRecaudado = "SELECT sum(Total) as suma FROM tb_ventas WHERE Fecha >= '$hoy' and idUsuario = '$idUser'";
			$queryRecaudado = $this->db->query($sqlRecaudado);
			$recaudado=$queryRecaudado->row();
			$recaudado=$recaudado->suma;
			if($recaudado<=0)$recaudado=0;
			return $recaudado;
		}
	}


	function getVentasPeriodo($fecha1="",$fecha2="")
	{
		$sql = "SELECT  vts.id,vts.idUsuario,vts.Total,vts.Fecha,u.nombre as nombreUsuario, vts.descuento
		FROM tb_ventas vts
		inner join tb_usuarios u on vts.idUsuario = u.id
		WHERE vts.Total>0
		and  vts.Fecha > '$fecha1 00:00:00'
		and vts.Fecha<='$fecha2 23:59:59'";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	/*
	 * Consulta de ventas por periodo y por datos de cliente
	 */
	function getVentasCliente($fecha1="",$fecha2="", $cliente){
		$cadenaFechas = "";
		if ( $fecha1 != "" && $fecha2 != "" ){
			$cadenaFechas = "AND  vts.Fecha > '$fecha1 00:00:00' AND vts.Fecha <= '$fecha2 23:59:59'";
		}
		$sql = "SELECT vts.id,
			vts.idUsuario,
			vts.Total,
			vts.Fecha,
			u.nombre AS nombreUsuario,
			c.nombre,
			c.apellido,
			c.direccion,
			c.nitci,
			c.id AS idcliente
			FROM tb_ventas vts
			INNER JOIN tb_usuarios u ON vts.idUsuario = u.id 
			INNER JOIN tb_clientes c ON vts.idCliente = c.id
			WHERE vts.Total > 0 ".$cadenaFechas." 
			AND c.nombre LIKE '".$cliente['nombre']."%'
			AND c.apellido LIKE '".$cliente['apellido']."%'
			AND c.direccion LIKE '".$cliente['direccion']."%'
			AND c.nitci LIKE '".$cliente['nitci']."%'";
		$query = $this->db->query($sql);
		return $query->result_array();
	}



	public function getCantidadByProducto($idProducto=0)
	{
		$sql = "SELECT cantidad FROM tb_inventario
		WHERE id='$idProducto'";
		$query = $this->db->query($sql);
		$r=$query->row();
		return $r->cantidad;
	}

	public function getMonedaString()
	{
		$sql = "SELECT monedaString FROM tb_config";
		$query = $this->db->query($sql);
		$r=$query->row();
		return $r->monedaString;
	}

	function getTipo($id)
	{
		$sql = "SELECT * FROM tb_tipos WHERE tb_tipos.id = ".intval($id);
		$query = $this->db->query($sql);
		return $query->row_array();
	}

	/**
	 * obtener la lista de ventas que 
	 * se realizaron a credito
	 * @return array
	 */
	public function getVentasACredito(){
		$sql = "SELECT tb_ventas.`*`, tb_usuarios.nombre as nombreUsuario, tb_clientes.nombre as nombreCliente FROM tb_ventas 
			INNER JOIN tb_usuarios ON tb_ventas.idUsuario = tb_usuarios.id
			INNER JOIN tb_clientes ON tb_ventas.idCliente = tb_clientes.id
			WHERE tb_ventas.cobro_credito = 1
			AND tb_ventas.estado = 0";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	/**
	 * obtiene un array de pagos 
	 * realizados de una venta
	 * @param int $idVenta
	 * @return array
	 */
	public function getPagosVenta($idVenta){
		$sql = "SELECT * FROM tb_venta_pagos WHERE idVenta=".$idVenta;
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	/**
	 * obtiene el total de pagos 
	 * realizados por venta
	 * @param int $idVenta
	 * @return array
	 */
	public function getTotalPagosVenta($idVenta){
		$total = 0;
		$arrayPagos = $this->getPagosVenta($idVenta);
		foreach ($arrayPagos as $key => $pago) {
			$total += $pago['monto'];
		}
		return $total;
	}


}
?>
